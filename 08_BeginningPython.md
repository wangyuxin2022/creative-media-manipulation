# Python and text, or learning how to learn to program

<!-- python, indentation, data types, knowing what you already know, reading in files, lists, split, sorting, loops, printing -->

I'm going to through you all for a loop now. We're going to move to a new programming language for a couple of weeks, Python. The reason why I'm doing this is not simply to make your life difficult. But rather it's because I want you to *learn how to learn to program*. At this point in your programming journey you have a good idea of the basics of programming: variables, conditionals, loops, functions, classes and objects, and arrays. And you've learned those concepts through a language that, while based on Java, is still somewhat simplified. That's been great for your learning process. But once you leave this class, you will likely want to do things that would be quite difficult to do in Processing. One thing that Processing doesn't handle well is text analysis and manipulation. As well, it's not a language that can easily be used to interact electronics hardware like an Arduino or Lilypad. So it's best to know what tools exist that are *better* for different kinds of work. And sometimes you'll need to learn a whole new language from scratch just to do one thing. I want you to feel the accomplishment that comes from knowing that you know how to learn how to learn to program.

## Python: a scripting and interpreted language

There are basically two kinds of programming languages: compiled and interpreted[^bytecode]. In the first, of which the languages C and C++ are examples, the human-written code is *compiled* to a machine-specific, machine-readable code. This step can take a fair bit of time to complete, although it usually produces extremely efficient programs. Compiled languages have been the standard since Grace Hopper wrote some of the first compilers in the 1950s. But in recent decades there has been much growth in *interpreted* languages, which don't need to be compiled into machine code, but that can be run immediately through an *interpreter*. This usually leads to code that doesn't run as fast or is as efficient as compiled code, but at the same time interpreted code can be *much* quicker to write, as the programmer doesn't have to wait for the compiler to do its work. As well, the programmer can test out different programming approaches through the interpreter in *real-time* without having to go through a compile-run-debug cycle.

[^bytecode]: Java, and Processing with it, is a third-kind. Java programs are meant to be runnable on a variable of platforms, so they are first compiled to an intermediate step, called *bytecode*, which then can run on a variety of platform-specific *virtual machines*. The details of this get somewhat complicated, so I'll just leave it at that.

Python is also known as a *scripting* language, meaning that it was originally designed to do, primarily, simple programming tasks from the command-line^[The *command-line* refers to a text-based interface to your computer, where you type all of your commands through the keyboard, rather than using your mouse to point-and-click. The command-line is *vastly* faster for certain kinds of tasks, and often utilizes small programs that can be chained together to do complicated things. While the command-line has fallen out of favor since the rise of graphical user interfaces (GUI) since the late 1980s and early 1990s (like Windows and MacOS), the command-line is still favored for many tasks by programmers and people like your instructor.] that were written as a sequence of commands in a file. Python has expanded far beyond this, but note that basically everything that we type into a file in our IDE we could just as easily type into the interpreter and run there. There is no conceptual difference.

## PyCharm and the IDE

We're going to use what's called an *integrated development environment*, or IDE, to work with Python. You've already used an IDE, namely the Processing IDE. An IDE brings together a text editor (where you write the code and the syntax is properly highlighted), automated tools for checking syntax (which the Processing IDE did), and a way to run and test your code. The Python IDE we will use will do similar things.

Now, if you are on OS X or Linux, Python will be installed by default. We could run all of our code from the *command line* or *terminal*, given that Python is a scripting language with an interpreter. All we need as a way to edit our text in some sort of plain-text editor and the interpreter itself. But to do so is more work than we'd like, and doesn't allow for the rich integration of features that an IDE has.

The Processing IDE is designed for complete beginners to programming, which is why it has a relatively simple, uncluttered interface. But most IDEs aren't that way, unfortunately, as they're designed for professional programmers who need advanced features to work with their code. There's not much we can do to get around that, in this case, so there will be a lot of the interface that you will just need to ignore.

IDEs come and go, but right now the best one for our purposes is something called PyCharm (https://www.jetbrains.com/pycharm/). It is freely downloadable in its "Community" edition, and is open source as well[^opensource]. Download and install this software.

[^opensource]: What "open source" means can be a complicated rabbit hole to fall down. But basically it means that the source code for the software is available for anyone to look at (to build upon, to improve, to search for bugs or security issues). This is in comparison to *proprietary software*, like Adobe's programs, or the Microsoft or Apple operating systems. Much of the world of computing runs on open source software, including web browsers, parts of your laptops and phones, and many of the databases that store information.


![Creating a new Python virtual environment.](media/PyCharm_NewProject.png){#fig:newproject}

Here's an additional wrinkle that I need to explain. The contemporary standard practice in Python development is to *isolate* the Python code that you're writing into what's called a *virtual environment*. This allows you to keep the specific versions of Python that you're using, as well as any other libraries that you might use, separate from other Python projects that you're working on. All this means in our case is that setting up a new project in PyCharm (which you do by going to "File -> New Project..."; see Figure @fig:newproject) means that you're also setting up a new virtual environment, abbreviated as `venv`. This is a new folder structure setup for each new project that contains a lot of files that you don't need to worry about for now. All you need to know is that to create a new Python file in PyCharm is that you click on the `venv` folder in the left folder pane of the interface, and then right click (or go to "File -> New..."), select "Python File", and then give your new Python file a name, which you can now edit. This file will then open in the right hand side of the interface where you can edit your code.

## Python datatypes

Python has a number of the same datatypes as Processing (and most other languages), so I won't be spending too much time going into them. The best explanation for these datatypes is through looking at examples, so after a brief overview, we'll quickly jump into their actual use.

Two main things to note: 

* **Every variable in Python is itself an object**. That bears repeating: **every time you declare a variable in Python, you're also instantiating an object of that type**. So, that means that your `int()`s have methods that you can call on them, as well as your `str()`s (strings) and `list()`s (arrays) too. You'll be using these methods especially with strings and lists and dictionaries (which we'll come to in a moment), as they offer much of the power of Python.
* Variables can be defined wherever you like in your code, and they don't need to be prefixed by a type. Python figures out the type from context. Of course, if you want to you can be explicit about the type (by calling one of the type functions). But you don't need to do it the vast majority of the time.

For more on the datatypes in Python, you can look at the [Python tutorial](https://docs.python.org/3/tutorial/datastructures.html).

### `int()` and `float()`

Ints and floats are defined pretty easily:

```python
intval = 3
floatval = -5.68794

floatvalConverted = int(floatval)
intvalConverted = float(intval)
```

Conversion from `float` to `int` occurs like in Processing, where the values past the decimal point are truncated.

### `bool()`

Boolean values are represented as `True` or `False`:

```python
raining = True
sun = False
```

You can of course use boolean values in your conditionals, as we'll see in a bit.

### `str()`

Strings can be defined in a straightforward manner.

```python
hello = "Hello, world!"
strnum = str(123) # This now holds the string representation of the number 123

## We can call specific methods on the string; 
## this converts the string to all lowercase
helloLowercase = hello.lower() 
```

There are many, many different and useful methods for the `str()` object, which we'll go into below.

### `list()`

In Python you call an Array a `list()`. Lists can be instantiated very easily within square brackets.

```python
intlist = [1, 4, -9, 2, 3]
strlist = ["Some", "string", "with", "some", "words"]
mixedlist = ["Here", "we", "have", 45, "some", -2.345]

## Adding new elements to the end is easy
mixedlist.append("five")

## Getting values from the array is like you know
strvalue = mixedlist[2] # "have"
strvalue = mixedlist[3] # 45
intvalue = mixedlist[-4] # 45, you can also access things using negative indices, going from the end of the list
```

Python lists can include mixes of different types. These lists are also *mutable*, meaning they can be easily changed, with elements being able to be removed, added, modified, etc. A Python `list()` also retains its order, like arrays in Processing. `list()`s also have a variety of useful methods that one can use on them, and we'll go into some of those methods in a bit.

There are other `list()`-like types in Python, such as `tuple()` and `set()`, but we won't be going into detail about them right now.

### `dict()`

The new datatype we're going to learn about is called a *dictionary* (`dict()`). A dictionary associates a *key* with a *value*. Dictionaries are also called *hash tables* or *associative arrays* in other languages. The idea here is to provide a (sometimes) human-readable key that can reference a particular value. Like with `list()`s, the values in Python dictionaries can be mutated while your code is running, and you can easily add new keys to the dictionary whenever you'd like. Only *immutable* types can be keys for Python dictionaries, so you should only use strings or numbers as keys. As well, keys must always be *unique* within any given dictionary.

```python
mydict = {"Hello": "goodbye",
        "one": 2,
        "five": 23.4568,
        2: "wonder"}

## Adding a new key is easy
mydict["once"] = "upon a time"

## And getting elements is straightforward too
strvalue = mydict["Hello"] # "goodbye"
```

Dictionaries have easy methods to get a list of keys and/or values:

```python
## Assume we have the dictionary declared before
listofkeys = mydict.keys()
listofvalues = mydict.values()
```

Thus, you can easily iterate over the values in your dictionary by iterating over the keys and then accessing the values. We'll look at an example of that in a bit.

There's a very important thing to note: dictionaries are not stored in any particular order, unlike lists! That means you can't rely on the order in which you *add* things to a dictionary to be preserved. This will likely come to bite you in the future, so try and keep it in mind! Think of a dictionary as an *unordered* set of *key-value pairs*.

## Blocks

Here's the thing that you might come to love or loathe about Python. Instead of defining blocks with curly brackets, as in Processing, you instead define blocks by *indentation level*. That is, if you start a block with your text beginning in column 0 (no tabs or spaces indented), you need to *indent* by a *consistent amount* (almost always four (4) spaces) in all of the statements within your block. An example should help explain this, even though we haven't gone into conditionals yet:

```python
## Consider the booleans we defined above
if raining:
    print("It's raining!")
```

So, in this case, we indented by four spaces when we wanted to `print()` out a string. (We'll get into `print()` in more detail in a bit, but I'm betting you understand a bit how it works!)

What happens if you need to *nest* blocks?

```python
if raining:
    print("It's raining!")

    if not sun:
        print("The sun isn't shining :-(")
```

Yup: to have a nested block, you then have to indent *eight* spaces. The conditional about `sun` only runs when `raining` is `True`.

This method of defining blocks might look really wonky. But it allows for easy-to-read code, and it immediately forces a programming style.

But! It's *extremely* easy to mess up inadvertently. Thus, the reason why we're using an IDE, which helps us keep track of these spaces and blocks. If you don't use an IDE, you're going to hate me for asking you to learn Python...so please use the IDE?

As well, if you're confused as to why your code isn't running properly, ensure that things are properly indented to the level needed for whatever it is you're trying to do.

## Conditionals

You already saw a bit about conditionals in the previous example. Conditionals in Python use standard English words to do Boolean logic operations like `and` and `or` and `not`:

```python
raining = True
sun = False
snowing = False

if (raining or snowing):
    print("Wetness in April, I guess.")
elif sun:
    print("It's actually sunny out!")
else: 
    print("Who knows what the weather is like today")

if raining and not sun:
    print("Can't see the sun through those rainclouds!")
```

Maybe you can see the gist here. You don't have to enclose your *condition* within parentheses, but I almost always do in my own code out of habit. To get an *else if* in Python, you use `elif` instead. And, you can chain `and`s and `or`s and `not`s just like you would in Processing. But again: it's sometimes best to be explicit about your conditions by actually using parentheses, rather than leaving it up to chance with Python.

## Loops

Python is in love with loops. Loops are one of the main things that Python does *extremely* well in terms of its syntax. See below:

```python
mixedlist = ["Here", "we", "have", 45, "some", -2.345]

for item in mixedlist:
    print(item)
```

Remember the loop in Processing that I called a "foreach" loop? In Python, basically *all* loops are foreach-style loops. In the case above, I have a list, `mixedlist` that I want to iterate over. In my loop I say, "go through each element of `mixedlist`, and each time through the loop assign that element to the local variable `item`". You can read the loop also as: "for each `item` in `mixedlist`, do something". Note also the indentation, since we're starting a new block (just like in Processing).

Python doesn't have an exact equivalent to the first loop we learned about in Processing, the `for (int i = 0; i < 10; i++)` style of loop. But, you can use a special Python function called `range()` that returns a list of ints between two values to create the equivalent loop:

```python
for i in range(0, 10):
    print(i)
```

This is very much a Pythonic idiom that you will see quite regularly.

`range()` also has an optional third argument, `step`, that allows you to increment (or decrement) by values other than 1. Thus, to re-create our loop from the parametric lines assignment, you'd write the following:

```python
for i in range(0, 50, 5):
    print(i)
```

There's more to loops than this, which we'll get to in due course.

## Aside: modules and scripts

One more important thing about Python to explain before we go into a series of examples. Remember when we finished up with Processing that we were using a number of *external libraries* that we had to `import` into our code. Writing libraries, or *modules* in the terminology of Python, is quite a common thing to do. In fact, we're going to be writing our own module in just a bit, so we'll see an example of how this works. However, recall that Python is a *scripting* language, which means its code is often called from the *command-line* as a script. This means that we need a way to distinguish between the code that should be loaded when we *import* a Python module to use it as a library, and the code that should be executed when we *run* the script from the command-line, as we often don't want to run that code when we're just importing the module.

Enter the `__name__` variable. This is a *special* variable in Python, which we know because it has two underscores each as prefix and suffix. When our code is run as a *script*, from the command-line, the `__name__` variable is set by the Python interpreter to be the string `"__main__"`. Thus, we can write a conditional that checks what `__name__` is set to; if it's set to `"__main__"`, then we know we're being run as a script, and we can write our code that should be executed. All code within this block will then *not* be run when we `import` the file as a module.

This might not make a lot of sense right now, but it'll become clear in a bit.

So, to actually do this, we can write something like the following:

```python
val1 = 23
val2 = -67.456

if __name__ == "__main__":
    if val1 > val2:
        print("val1 is greater than val2")
```

Thus, the string `"val1 is greater than val2"` will only be printed out when this file is run as a script, and not when it is loaded as a module.

## String substitutions

Python makes it very easy to manipulate and substitute one string for another. Let's look at an example, and see if you can figure out what's going on based on what you already know about programming:

```python
name = "Adriana"
print("Hello, %s" % name)
```

What do you think is happening? This is an example of *string substitution*. We have a string enclosed in double quotes with a *formatting string* within it, `%s`. This is saying to Python to expect another string (hence the "s") to be *substituted* where it sees the characters `%s`. Then, we have another percent sign to set off the name of the variable that will be substituted.

Let's look at another example:

```python
tempval = 45
print("It's %s degrees outside." % tempval)
```

You can use any datatype you'd like, with the `%s` formatting string, and Python will try and format it as a string.

If you want to use multiple substitutions in a string, you can do that, you just have to use multiple formatting strings, and enclose the multiple substitutions within parentheses:

```python
temp = "warm"
weather = "raining"
print("It's both %s and %s outside." % (temp, weather))
```

What if you have more than one variable that you'd like to format within a string?

```python
intval = 5
floatval = 3.5678
print("The integer is %d and the float is %f" % (intval, floatval))
```

In this case we're using two new formatting strings. The first, `%d`, tries to format a variable as a *decimal* value, and the second, `%f`, tries to format the string as a *float* value. The two variables to be substituted are within a set of parentheses.

## Some longer examples

Here's a whole set of examples using the datatypes, conditionals, and loops that we've just gone over. First, the variable declarations:

```python
"""
In this file we're going to go through some of the basic python syntax.
A lot of this is might begin to look familiar to you from your experience
with Processing.
There are definitely some specific Python ideosyncrasies that we'll go
into in depth, however.
"""

## In Python there *are* specific datatypes, but you don't declare them
## when you define your variable. As well, you can declare varaibles wherever
## you'd like in your code.
## Basic Python datatypes include ints, floats, booleans, strings, tuples,
## sets, lists, and dictionaries. We're not going to worry too much about
## tuples and sets.
## You've seen all of the other datatypes in Processing, excepting dictionaries.

## This means that you can define variables where you like, and without needing
## to explictly say what they are. The type is inferred from context.

## This would be an int
xpos = 5

## This a float
randomval = 4.357684593

## Booleans are either `True` or `False` (note the capital letters!)
raining = False

snowing = False

sun = True

## Strings are declared very easily
hello = "Hello, world!"

## Lists are like arrays, and they are enclosed in square brackets.
## Also, strings exist as their own datatype and are enclosed in double quotes.
towns = ["Wellesley", "Natick", "Needham", "Boston", "Cambridge"]

## Lists can include unlike datatypes.
values = [3, -4.5679, True, "What's this?"]

## Dictionaries are a new datatype you haven't seen before.
## In a dictionary you associate a "key" with a "value". Each "key"
## can only refer to a *single* value. Dictionaries have no particular
## order to them; that is, unlike a list, you can't assume that items in a
## dictionary will be in the same order each time you access it.

## See https://www.youtube.com/watch?v=wa2nLEhUcZ0 for context
thoughts = {"Monday": "blue",
            "Tuesday": "gray",
            "Wednesday": 2,
            "Thursday": "don't care",
            "Friday": "love"}

## Dictionaries can also include other datatypes
combo = {"a list": towns,
         "days": thoughts,
         5: 20,
         "a really long key that I wouldn't recommend using but you can": values}

## Each of the datatypes as a corresponding `function` that you can use to
## convert one kind of datatype to another. Note that this won't always do what
## you want!
## These are the datatype functions:
## int, float, bool, str, list, dict
intval = int(4.356784)
floatval = float(3)

## Now, here's another thing about Python: **everything** is an object!
## That means your intval is an object. Your floatval is an object. And, like
## Processing, your strings are objects too. As well as your lists and dicts.
## Which means you can sometimes see something wonky like the following,
## which we'll explain in a bit:
sentence = " ".join(["This", "is", "now", "a", "sentence."])
```

And now, here're the variables used in context. Note the `__name__` idiom that we learned about earlier:

```python
## Now, let's go through how to do things with these variables.
if __name__ == "__main__":
    # You can print out values just like you did in Processing.
    # By default, `print` also includes a newline at the end of each statement.
    print("PRINTING VALUES")
    print(xpos)
    print(randomval)

    print("\n")

    # Blocks are defined by spaces, not by curly brackets.
    # You can do boolean tests with English words, rather than the symbols
    # used in Processing
    print("WORKING WITH BOOLEANS")
    if ((not raining) and (not snowing)):
        print("Wow, it's not raining nor snowing???")

    # The following is *also* legit Python code, although I almost always prefer
    # to use parentheses to be explicit about things.
    if not raining and not snowing:
        print("Wow, it's not raining nor snowing?")

    print("\n")

    # Booleans work like you would expect
    if (sun):
        print("The sun is shining")

    print("\n")

    # Just like normal
    if (xpos > randomval):
        print("That value (%d) is larger than the other value (%f)" % (xpos, randomval))

    print("\n")

    # You can print strings like you would expect
    print("PRINTING STRINGS")
    print(hello)

    print("\n")

    print("WORKING WITH LOOPS")
    for town in towns:
        print(town)

    print("\n")

    print(towns[3])

    print("\n")

    for town in towns:
        print(town.upper())

    print("\n")

    for value in values:
        print("Value: %s" % value)

    print("\n")

    # Python doesn't have a specific syntax for the `for (int i = 0; i < 10;
    # i++)` kind of loops. So we use a function called `range()` that
    # takes a lower and upper limit to produce a list of indices.
    print("WORKING WITH LOOP INDICES")
    for i in range(0, 10):
        print("Current index is %d" % i)

    print("\n")

    # One thing to note! You can think of strings as lists of characters
    # too, # just like we discussed when we learned about them in Processing.
    # Thus, you # can use the syntax you learned just before with lists to
    # access individual characters in the string.
    print("WORKING WITH STRING INDICES")
    print(hello[3])

    print("\n")

    # And, you can use *negative* indices to start counting from the *end* of
    # the string. This will be helpful at times.
    print(hello[-4])


    print("\n")

    print("WORKING WITH DICTIONARIES")
    for key in thoughts.keys():
        print(thoughts[key])

    print("\n")

    for key in combo.keys():
        print("Key (%s) and value (%s)" % (key, combo[key]))

    print("\n")

    print(sentence)
```

And this is the output of the previous code:

```
PRINTING VALUES
5
4.357684593


WORKING WITH BOOLEANS
Wow, it's not raining nor snowing???
Wow, it's not raining nor snowing?


The sun is shining


That value (5) is larger than the other value (4.357685)


PRINTING STRINGS
Hello, world!


WORKING WITH LOOPS
Wellesley
Natick
Needham
Boston
Cambridge


Boston


WELLESLEY
NATICK
NEEDHAM
BOSTON
CAMBRIDGE


Value: 3
Value: -4.5679
Value: True
Value: What's this?


WORKING WITH LOOP INDICES
Current index is 0
Current index is 1
Current index is 2
Current index is 3
Current index is 4
Current index is 5
Current index is 6
Current index is 7
Current index is 8
Current index is 9


WORKING WITH STRING INDICES
l


r


WORKING WITH DICTIONARIES
blue
gray
2
don't care
love


Key (a list) and value (['Wellesley', 'Natick', 'Needham', 'Boston', 'Cambridge'])
Key (days) and value ({'Monday': 'blue', 'Tuesday': 'gray', 'Wednesday': 2, 'Thursday': "don't care", 'Friday': 'love'})
Key (5) and value (20)
Key (a really long key that I wouldn't recommend using but you can) and value ([3, -4.5679, True, "What's this?"])


This is now a sentence.

Process finished with exit code 0
```

Note that last line. That is some information from our IDE that our code completed successfully. It's telling us that the *return code* of our script was 0, which indicates that everything worked like it should have, i.e., there were no errors. This touches upon the *shell* and how the shell deals with *processes* running like Python, so we won't go into it in too much detail. Suffice it to say, nevertheless, that this code runs like it should!

I hope that you feel like you actually know a fair bit about how to program in Python, given your experience with Processing, and even though it's a new language (to most of you)!

## Comments

To write a single-line comment in Python you use the pound/hash symbol:

```python
## This is a really useful comment about variables
var = 5
```

Multiline comments are given within *triple-quoted strings*:

```python
"""
This is a comment

that spans

a number of lines and maybe

or maybe not

is useful to explain the following code.
"""
fval = 23.693
```

Multiline comments are used extensively in *docstrings*, something we'll look at when we get into classes below.

## Functions

Functions are defined rather simply:

```python
def hello_world():
    print("Hello world!")
```

Since defining a function starts a new block, you have to indent all of the code within the function by a *minimum* of four spaces.

You can also pass arguments to functions. Like everything else in Python, you don't have to declare the type of the argument:

```python
def hello_world_name(name):
    print("Hello, %s" % name)
```

You could call this function in a piece of code as follows:

```python
hello_world_name("Adriana") # Prints out "Hello, Adriana"
```

You can also have functions that return values, of course:

```python
def mult(x, y):
    val = x * y
    return val
```

Thus, you can call this function like:

```python
newval = mult(3, 4.56787)
```

Of course, if you were to pass a *string* to the `mult()` function, strange things might happen. Python won't *prevent* you from doing that. And it *might* work depending on the type of the other variable. But this is something that you, as the programmer, have to keep track of. It's one of the downsides to using an *interpreted* language versus a compiled one.

But the main new thing about Python functions are *default* or *optional* arguments. Look at the following and see if you can guess what might be going on:

```python
def newmult(x, y, z = 10):
    val = x * y * z
    return val
```

And let's look at an example:

```python
val = newmult(2, 3) # What is val?
val = newmult(2, 3, 5) # What is val now?
```

Remember back to when we were talking about function *overloading* in Processing? This is the way in which Python accomplishes the same thing. You can define *default* values for certain arguments in your Python function, which then makes those arguments *optional*. But, your optional arguments **must** come *after* the required arguments. You can have as many optional arguments as you would like.

That's about it for functions! Of course functions can include conditionals, loops, and any datatype imaginable. We'll see some examples going forward of this.

## Classes

Given that *everything* in Python is an object, it won't be surprising that writing classes is something you do a lot of in Python. And it's here we we can finally understand a bit more about Python modules.

Let's say that we want to write a class that allows us to store information about the computers we own. We'd like to be able to save info about the computer's name, it's type, how fast it is, who made it, and so on. In Python, classes are defined using the `class` keyword, then the name of the class (also Capitalized as a convention, like Processing), then the word `object` in parentheses, and finally a colon:

```python
class Computer(object):
```

The word `object` here might be curious. We haven't talked about *class inheritance* yet, and we won't be going into it in much detail. But the idea is that you can write a class that *builds upon* another class, and thus *inherits* that classes variables and methods. It's a fairly complicated, yet powerful, way of building classes. In Python, all classes ultimately inherit from the most basic class named `object`; here we're just making that explicit.

We then indent in order to start writing the class body, just like we did in Processing. Our first part of the class is something we call the *docstring*, which is a triple-quoted string that provides a human-readable text of what the class is meant to do. This will be useful to us when we talk about the different ways we can *get help* in Processing. After the docstring comes the class variables, just as in Processing:

```python
class Computer(object):
    """This is a toy class for defining a computer."""

    name = ""
    computerType = ""
    manufacturer = ""
    speed = ""
    memory = 1024
```

Now we get to the constructor, which in Python is called the `__init__()` function:

```python
    def __init__(self, name = "Computer", computerType = "PC",
                 manufacturer = "Lenovo", speed = "fast", memory = 2048):
        """Initialize our class."""
        self.setName(name)
        self.setComputerType(computerType)
        self.setManufacturer(manufacturer)
        self.setSpeed(speed)
        self.setMemory(memory)
```

A couple of things to note:

* See the word `self` as the first argument? Remember back to our discussion of `this` in Processing. In Python, `self` works the same way. It's a way of referring to the class itself. **Every function** within your class needs to have `self` as the first argument.
* I have made every single argument to the `__init__()` function an optional argument. This takes care of the question of overloading the constructor that we talked about in Processing. Thus, I can initialize a `Computer` object with any combination of arguments, from none of them to all of them.
* In the constructor itself, I call various *class methods* by using the idiom `self.nameOfMethod()`. This tells the class that I want to refer to the method `setName(name)` that is written *in the class itself*, rather than some other method that might be floating elsewhere in my code.

Writing specific class methods is straightforward once you understand how functions work and the idiom of using `self` as the first argument:

```python
    def setName(self, name):
        """Set the name of the computer."""
        self.name = name

    def setComputerType(self, computerType):
        """Set the computer's type."""
        self.computerType = computerType

    # ...

    def getSpeed(self):
        """Get the computer's speed."""
        return self.speed

    def getMemory(self):
        """Get the computer's memory (in megabytes)."""
        return self.memory

    def printKind(self):
        """Print out what kind of computer it is."""
        kind = ""
        if (self.getComputerType() == "PC"):
            kind = "You have a PC, it probably runs Windows or Linux."
        elif (self.getComputerType() == "Mac"):
            kind = "You have a Mac, it probably runs OS X."
        else:
            kind = "I don't know what you have!"

        return kind
```

Here's the full class definition:

```python
class Computer(object):
    """This is a toy class for defining a computer."""

    name = ""
    computerType = ""
    manufacturer = ""
    speed = ""
    memory = 1024

    def __init__(self, name = "Computer", computerType = "PC",
                 manufacturer = "Lenovo", speed = "fast", memory = 2048):
        """Initialize our class."""
        self.setName(name)
        self.setComputerType(computerType)
        self.setManufacturer(manufacturer)
        self.setSpeed(speed)
        self.setMemory(memory)

    def setName(self, name):
        """Set the name of the computer."""
        self.name = name

    def setComputerType(self, computerType):
        """Set the computer's type."""
        self.computerType = computerType

    def setManufacturer(self, manufacturer):
        """Set the computer's manufacturer."""
        self.manufacturer = manufacturer

    def setSpeed(self, speed):
        """Set the computer's speed."""
        self.speed = speed

    def setMemory(self, memory):
        """Set the amount of memory the computer has (in megabytes)."""
        self.memory = memory

    def getName(self):
        """Get the computer's name."""
        return self.name

    def getComputerType(self):
        """Get the computer's type."""
        return self.computerType

    def getManufacturer(self):
        """Get the computer's manufacturer."""
        return self.manufacturer

    def getSpeed(self):
        """Get the computer's speed."""
        return self.speed

    def getMemory(self):
        """Get the computer's memory (in megabytes)."""
        return self.memory

    def printKind(self):
        """Print out what kind of computer it is."""
        kind = ""
        if (self.getComputerType() == "PC"):
            kind = "You have a PC, it probably runs Windows or Linux."
        elif (self.getComputerType() == "Mac"):
            kind = "You have a Mac, it probably runs OS X."
        else:
            kind = "I don't know what you have!"

        return kind
```

Using this class to instantiate objects is straightforward too:

```python
c1 = Computer(name = "redstar", computerType = "Mac", manufacturer = "Apple")
print(c1.getName())
print(c1.printKind())
```

This should make sense given what you know about classes from Processing.

Let's say that you want to use this class in a *different* Python file. That means we have to *import* this class as a module. Let's say that we saved our class `Computer` in a file called `Computer.py` in the same folder as all of our other code. To use this file as a module in another Python script, we can write:

```python
import Computer
```

Then, in our code, in order to instantiate an object, we'd write

```python
c2 = Computer.Computer(name = "wired", computerType = "PC")
print(c2.getName())
```

Can you see what's going on here? The first line of that code says, "we have a module called `Computer`, and in that module is a class called `Computer`, that we're going to use to instantiate an object called `c1`". So the dot syntax here works like `module.className`.

We can get rid of that extra typing by writing this at the top of our code instead:

```python
from Computer import Computer
```

That line says: "from the module `Computer` import the class named `Computer`".

To instantiate a class we'd then write:

```python
c3 = Computer(name = "stary", computerType = "PC")
```

So now you know pretty much the basics of writing Python code! We're going to move into more specific examples of use, learning some of the Python standard library. But first, we need to learn how to find help.

## Running code in PyCharm

![Running the current script from the "Code" menu."](media/PyCharm_run.png){#fig:run}

![The output of the current script, show in the console at the bottom of the window.](media/PyCharm_ConsoleOutput.png){#fig:runOutput}

To actually run your code in PyCharm, you can either right click on the code window, or go to the "Code" menu, and select "Run `name of script`", where `name of script` refers to the script file you're wanting to run. The output of the script, if any, is shown in the output window at the bottom of the screen. See Figures @fig:run and @fig:runOutput for details.


## Getting `help()`, or working with the interpreter

Given that Python is an *interpreted* language, we can run and test code using the Python *interpreter*. From the command-line (using a program like `Terminal` on OS X), you could just type `python` to call and enter the Python interpreter. In PyCharm, we can get to the interpreter, called the *console* here, by going to the "Tools" menu and selecting "Python Console..."; see Figure @fig:consoleMenu .

![Starting the Python interpreter (console) in PyCharm.](media/PyCharm_Interpreter.png){#fig:consoleMenu}

Once you've started the console, you can type Python code into it, just as if you were writing a Python script (Figure @fig:consoleCode ).

![Running code in the interpreter.](media/PyCharm_InterpreterCode.png){#fig:consoleCode}

One of the most useful functions within the console is `help()`. You can call help on any Python function or class to get more (or less, sometimes!) detailed help on how to use the classes and functions. For example, typing `help(str)` will give you a list of all of the methods that are available to a `str` object (Figure @fig:help_str). There, you can see that there is a very useful method called `split()` which can split one long string into a list of strings based on a *delimiter* (Figure @fig:help_str_split).

![Getting help in the console.](media/PyCharm_help001.png){#fig:help_str}

![Help on the `split()` method of the `str` class.](media/PyCharm_help002.png){#fig:help_str_split}

## String functions

Here are some useful `str` functions in Python; use the `help()` command in the interpreter to learn what they do, and they them out on a sample string.

* str.split()
* str.upper(), str.lower()
* str.replace()
* str.join()
* str.find()
* str.startswith()
* str.strip()

Please make note of methods that *return* a string, and those that change the string *in place*, meaning a modifying of the string without returning a new string. So, sometimes you have to assign the result of a `str` method to a *new* variable, and sometimes no new variable is required.

One important thing about strings is that they can be indexed into just like a list. Consider the following:

```python
hello = "Hello, world!"
print(hello[3])
```

Such a code will print out the character "l". Or, you could write:

```python
hello = "Hello, world!"
print(hello[3:8])
```

This would print "lo, w". So you can think of each character in a string as an element in a list, and use this technique of *string slicing* to "cut out" different parts of the string. Like with lists, you can also have *negative indices*:

```python
hello = "Hello, world!"
print(hello[-4])
```

which would print out the letter "r". (When using negative indices, you begin counting from "1" since index "0" is the *first* character of the string.)

These methods of slicing can also be used on Python lists.

## List functions

As we did with strings, use the interpreter to look up the help for the following `list` methods:

* `list.insert()`
* `list.pop()`, `list.append()`
* `list.extend()`
* `list.reverse()`

Also, try to play around with creating a list and doing different slicing operations on it. Please note as well, like with strings, some `list` methods *return* a new list, and some simply modify a list *in palce*.

## Sorting a list

Sorting a list is something that you will likely do quite often. Luckily there is a Python function called `sorted()` that allows you to pass a `list` to sort. The result will be a list sorted from low to high:

```python
l = [-4, 30, -20, 9, 1000, 3]
l_sorted = sorted(l) # [-20, -4, 3, 9, 30, 1000]
```

You can also sort lists of strings:

```python
s = ["this", "has", "Capital", "$38", "words", " ", "spaces", "mIxeD", "zebra"]
s_sorted = sorted(s) # [' ', '$38', 'Capital', 'has', 'mIxeD', 'spaces', 'this', 'words', 'zebra']
```

That sorting might look a little strange. But recall our discussion of ASCII characters from a few weeks ago. `sorted()` sorts based on the *ASCII value* of the first character in the string. So spaces sort before symbols before Capital Letters before lowercase letters.

You can reverse the sort (from high to low) by passing `reverse=True` as an optional argument.

`sorted()` is a very powerful function with many more capabilities than we can get into right now. See one of the examples for the possibility of using a different sort *key* than the default.

## Reading text files

One of the things you might also want to do is to read in a text file. In this case, we're going to use files downloaded from Project Gutenberg (http://www.gutenberg.org/), one of the oldest repositories of digitized texts, including novels, poetry, and non-fiction. We're going to download the *plain text* version of *Alice's Adventures in Wonderland* (1865), written by Lewis Carroll. You can download the "Plain Text UTF-8" version from here: http://www.gutenberg.org/ebooks/28885. 

Now we need to add this file to our virtual environment. In whatever project you're using, navigate to the virtual environment folder on your computer, through Finder (OS X), Windows Explorer (Windows), or some other method (if you're on Linux). Within the `venv` file, create a new folder called `data`; this will be at the same "level" as the folders `Include`, `Lib`, and `Scripts` that PyCharm already created. Move the downloaded plain text file to this new `data` directory[^data].

[^data]: It's not *required* to name your folder `data`, but it's a useful convention to use to help keep things organized.

It's now possible to load the file into Python[^file]. The current best practice for loading files safely into Python is to use the `with open() as f` syntax seen below:

[^file]: Right now we're only going to go over how to read in *plain text* files. However, Python can read in any kind of file, even *binary* ones where the structure is of some complex form. We're not going to go into that as it requires a fair bit of advanced knowledge about reading and writing files.

```python
lines = []
## This syntax ensures that the file is opened and closed properly
with open("data/AliceInWonderland.txt", "r") as f:
    # This reads each line of the text file separately, and adds it to a
    # list. Thus, `lines` contains every line of the text file as a
    # separate entry in the list.
    lines = f.readlines()
```

When you use the function `open()`, you pass as the first argument the *path* to the file to be opened (in this case, relative to the `venv` folder), and then in the second argument you pass the character "r" to indicate that you want to *read* the file. (There is a corresponding character "w" that you can pass to *write* to the file, and if you want to both read *and* write, you can pass "rw".) Then, the construct `as f` creates a *local variable* `f` that you then use in the block below. The variable `f` is an object that has access to an *IOstream* (input-output stream) that has a number of different methods that can be called on it. `f.readlines()` basically reads every line of the file into a separate element in a list. You can look at the Python help to see other methods that can be called.

Whenever you open a file you should also close it when you're finished. Using this syntax ensures that the file is properly closed once the code reaches the end of the `with...` block. 

And now you have a list of all of the lines of the file! You can do some interesting things with this text, like count how many words are in it, how many times each word occurs, and so on.

## Choosing an element randomly

Another common thing to do when working with lists is to select an element from the list at random. Luckily there's a package `random` that allows us to do that:

```python
import random
l = ["one", "that", "whose", "planet", "journey", often"]
random_string = random.choice(l)
```

## Exercises

Here are two exercises to try:

* Take some long string of text somewhere from the web. (**HINT**: It's probably best to save this as a multiline string.)  Split this text up into individual words. Try and sort the words alphabetically. Play around with printing these words out, changing their case, joining them back together again, and so on. And, as a final challenge, try and count how many times each word appears in your example text (i.e., the word "the" appears 8 times, the word "and" appears 5 times, etc.). **HINT** You'll probably want to use a `dict()` for this.
*  Write a poem generator that uses a set of lists of words and phrases, string substitution, and `random.choice()`. Your poem should be three lines long and involve at least 5 substitutions.
