"""
We're going to use the nltk library to tokenize our text, create bigrams
and trigrams, create a conditional frequency distribution, and to generate
some random text based on the distribution of text found in *Alice in
Wonderland*.
"""

import random

import nltk

# nltk.download(), nltk_book

from nltk.corpus import brown

def generate_text(cfd, start_word, num = 20):
    word = start_word
    all_words = "%s" % word

    for i in range(0, num):
        new_word = random.choice(list(cfd[word].keys()))
        all_words = "%s %s" % (all_words, new_word)
        word = new_word

    return all_words

def generate_text_from_trigrams(cfd, start_word, num = 20):
    word = start_word
    all_words = "%s" % word

    for i in range(0, num):
        possible_words = cfd[word]
        new_words = random.choice(list(possible_words.keys()))
        #new_words = random.choice(list(cfd[word].keys()))
        all_words = "%s %s %s" % (all_words, new_words[0], new_words[1])
        word = new_words[1]

    return all_words

def generate_trigram_cfd(trigram):
    cfd_dict = {}
    for item in trigram:
        first = item[0]
        rest = tuple((item[1], item[2]))

        if first in cfd_dict.keys():
            first_dict = cfd_dict[first]

            if rest in first_dict.keys():
                first_dict[rest] +=1
            else:
                first_dict[rest] = 1
            cfd_dict[first] = first_dict
        else:
            cfd_dict[first] = {}
            cfd_dict[first][rest] = 1

    cfd_dict_freqs = {}
    for key in cfd_dict.keys():
        bigrams = cfd_dict[key]

        # First pass, count up total number of occurences
        total = 0
        for bigram in bigrams:
            total += bigrams[bigram]

        # Then, calculate fraction
        for bigram in bigrams:
            bigrams[bigram] = bigrams[bigram]/total

        cfd_dict_freqs[key] = bigrams

    return cfd_dict

if __name__ == "__main__":

    print("ALICE IN WONDERLAND GENERATION")
    text = []
    with open("data/AliceInWonderland.txt", "r") as f:
        text = f.readlines()

    text = "".join(text)

    words = nltk.word_tokenize(text)

    sentences = nltk.sent_tokenize(text)

    bigrams = nltk.bigrams(words)

    cfd = nltk.ConditionalFreqDist(bigrams)

    print(generate_text(cfd, "Alice", num = 50))

    trigrams = nltk.trigrams(words)

    trigrams_cfd = generate_trigram_cfd(trigrams)

    print(generate_text_from_trigrams(trigrams_cfd, "Alice", num = 50))

    print("BROWN GENERATION")
    brown_words = brown.words()

    brown_trigrams = nltk.trigrams(brown_words)

    brown_trigrams_cfd = generate_trigram_cfd(brown_trigrams)

    print(generate_text_from_trigrams(brown_trigrams_cfd, "Alice", num = 50))
