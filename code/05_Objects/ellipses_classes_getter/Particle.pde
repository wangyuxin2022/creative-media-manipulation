class Particle {
  // Define our class variables
  float x = 0;
  float y = 0;
  float xoffset = 0;
  float yoffset = 0;
  float strokeR = 0;
  float strokeG = 0;
  float strokeB = 0;
  float fillR = 0;
  float fillG = 0;
  float fillB = 0;
  
  // Define our constructor
  Particle(float xinit, float yinit) {
    x = xinit;
    y = yinit;
    strokeR = 255;
    fillR = 255;
  }
  
  // Draw the particle to the screen
  void display() {
    stroke(strokeR, strokeG, strokeB);
    fill(fillR, fillG, fillB);
    ellipse(x, y, 20, 20);
  }
  
  // Update the particle's position
  void update() {
    xoffset = random(-7, 7);
    yoffset = random(-7, 7);
    
    // Check on the x coordinate
    if ((x + xoffset) < 0) {
      x = 0;
    } else if ((x + xoffset) > width) {
      x = width;
    } else {
      x += xoffset;
    }
    
    // Check on the y coordinate
    if ((y + yoffset) < 0) {
      y = 0;
    } else if ((y + yoffset) > height) {
      y = height;
    } else {
      y += yoffset;
    }
  }
  
  // Getter functions
  float getx() {
    return x;  
  }
  
  float gety() {
    return y;  
  }
}