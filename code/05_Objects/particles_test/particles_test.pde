Particle p1;
Particle p2;
Particle p3;

void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
  
  p1 = new Particle(width/2, height/2);
  p2 = new Particle(width/2, height/2);
  p3 = new Particle(width/2, height/2);
}

void draw() {
  background(0);
  
  p1.update(); // dot notation
  p2.update();
  p3.update();
  
  p1.display();
  p2.display();
  p3.display();
}