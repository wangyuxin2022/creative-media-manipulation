/*
 * Note: you do not need to try and understand this code
 * this week. I'm including this here to give you an idea
 * of where we're going with our Processing understanding,
 * and showing you the power of classes and something
 * we're going to learn next week, called Arrays.
 * Run this code to see it in action, and change the
 * value for the variable "count" to add more or fewer
 * Particles on the screen.
 */
 
Particle pArray[];

void setup() {
  // We're going to use variables below for setting the width
  // and height of each particle
  float w = 0;
  float h = 0;
  
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);
  
  int count = 200;
  
  pArray = new Particle[count];
  
  for (int i = 0; i < count; i++) {
    pArray[i] = new Particle(width/2, height/2);
    float r, g, b;
    r = random(0, 255);
    g = random(0, 255);
    b = random(0, 255);
    
    pArray[i].setStroke(r, g, b);
    pArray[i].setFill(r, g, b);
    
    w = random(10, 40);
    pArray[i].setWidth(w);
    pArray[i].setHeight(w);
  }
  
}

void draw() {
  background(0);

  for (Particle p: pArray) {
    p.update();
    p.display();
  }
}