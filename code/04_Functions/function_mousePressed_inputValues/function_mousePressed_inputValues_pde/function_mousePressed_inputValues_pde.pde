void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  if ((frameCount % 30) == 0) {
    hello_world();
  }
}

// This is our function that we're defining
// Take note of the syntax
void hello_world() {
  println("Hello, world: ", frameCount);
}

// This is a function that outputs the mouse coordinates 
// when the button is clicked
void print_mouse(int x, int y) {

  println("Mouse was clicked at: ", x, " ", y);
}

void mousePressed() {
  // Here was call our defined function
  print_mouse(mouseX, mouseY);
}