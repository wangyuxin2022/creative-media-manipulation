int i = 0;

void setup() {
  size(600, 600);
  background(0);
  smooth();
}

void draw() {
  fill(255);
  stroke(255);
  
  // We draw lines until our condition says that 
  // we've exceeded the height of the window
  while(i < height) {
    line(100, i, 500, i);
    i += 50;
  }
}