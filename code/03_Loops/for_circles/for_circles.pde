void setup() {
  size(600, 600);
  background(0);
  smooth();
}

void draw() {
  fill(255);
  stroke(255);
  
  // We draw circles from one side of the screen
  // to the other
  for (int i = 0; i < width; i++) {
    ellipse(i*20, height/2, 10, 10);
  }

}