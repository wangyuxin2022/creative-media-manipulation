void setup() {
  size(600, 600);
  background(0);
  smooth();
}

void draw() {
  fill(255);
  stroke(255);
  background(0);
  
  // We draw circles from one side of the screen
  // to the other, and from the top to the bottom
  for (int i = 0; i <= (width/20); i++) {
    for (int j = 0; j <= (height/20); j++) {
      ellipse(i*20, j*20, 10, 10);
    }
  }
}