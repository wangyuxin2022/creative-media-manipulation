int xPos = 0;

void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  
  // This is the new code
  // It uses the parameter frameCount to offset the y position
  // of the circle depending on the sin function
  for (int i = 0; i < 10; i++) {
    ellipse(xPos+(10*i), 10*(i+1)*sin(frameCount*0.009*(i+1)) + height/2, 10, 10);
  }
  
  // This is another way of writing
  // xPos = xPos + 1;
  xPos += 1;
  
  // Check that xPos is not past the edge of the screen
  // If so, reset it to 0
  if (xPos >= (width)) {
    xPos = 0;  
  }
}