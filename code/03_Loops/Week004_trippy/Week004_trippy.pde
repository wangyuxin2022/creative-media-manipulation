// Decide on the size of the squares
int squareW = 20;
int squareH = 20;

boolean fullcolor = false;

void setup() {
  size(600,600);
  // Slow our frame rate down a bit
  frameRate(30);
  smooth();
}

void draw() {
  background(0);
  
  // This for loop gets us the oscillations we want
  // We set our limits to be the *number of squares on the screen*
  // in the x and y dimensions.
  // Then we count from 0 up to that limit.
  // Then we use those loop variables in the `sin()` function.
  // If you count up to `width` or `height`, then you're going to get a lot of flickering.
  // Also, my suggested `frac` values were off by an order of magnitude :-); see below
  for (int i = 0; i < (width/squareW); i++) {
    for (int j = 0; j < (height/squareH); j++) {
      // Here's one way of doing the offset and `frac` values
      int r = int(128 + 127*sin(frameCount*(i+1)*(j+1)*0.0005));
      int g = int(128 + 127*sin(frameCount*(i+1)*(j+1)*0.0004));
      int b = int(128 + 127*sin(frameCount*(i+1)*(j+1)*0.0002));
 
      if (fullcolor) {
        stroke(r, g, b);
        fill(r, g, b);
      } else {
        stroke(r, r, r);
        fill(r, r, r);
      }
      
      // Draw the square based on our loop variables
      rect(i*squareW, j*squareH, (i+1)*squareW, (j+1)*squareH); 
    }
  }
  
  // This saves each frame to a PNG file if you want to make an animation
  saveFrame("trippy-######.png");
}

void keyPressed() {
  // We use `fullcolor` as a toggle
  if (key == 'f') {
    fullcolor = !fullcolor;
  }
}
