// Declare a PImage
PImage cosmos;

void setup() {
  // We can go back to our standard size
  size(600, 600);
  frameRate(30);
  smooth();

  // Load image from file
  // Image should already be in data directory.
  // That can be added using "Sketch -> Add File..."
  cosmos = loadImage("potw1010a_0_600px.jpg");
}

void draw() {
  background(0);
  
  // Display the image starting at the coordinates (0,0)
  image(cosmos, 0, 0);
}