/*
 * This is the code from last week with our Particle class
 * At this point you should be able to understand how it works
 */
 
// We declare our array of type Particle 
Particle pArray[];

// Boolean for shift being pressed
boolean shiftPressed = false;

void setup() {
  // We're going to use variables below for setting the width
  // and height of each particle
  float w = 0;
  float h = 0;
  float r, g, b; // this is a shorthand you might not have seen before
  
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);
  
  int count = 200;
  
  // And here we further declare our array of Particles
  pArray = new Particle[count];
  
  // We iterate over the array
  for (int i = 0; i < count; i++) {
    // Here's where we initialize the Particle class
    // for each element in the array
    pArray[i] = new Particle(width/2, height/2);
    
    // Choose random colors for the Particle
    r = random(0, 255);
    g = random(0, 255);
    b = random(0, 255);
    
    // Set the stroke and fill of the Particle
    pArray[i].setStroke(r, g, b);
    pArray[i].setFill(r, g, b);
    
    // Get a random width value
    w = random(10, 40);
    pArray[i].setWidth(w);
    pArray[i].setHeight(w);
  }
  
}

void draw() {
  // We need to clear the screen each frame
  background(0);

  // This is new syntax for iterating over an array
  for (Particle p: pArray) {
    // And this is all we need to draw things!
    p.update();
    p.display();
  }
}

void mousePressed() {
  // Create a new particle based on the position of the mouse
  Particle p = new Particle(mouseX, mouseY);
  
  // Local variables for colors, size
  float r, g, b, w;
  r = random(0, 255);
  g = random(0, 255);
  b = random(0, 255);
  p.setFill(r, g, b);
  p.setStroke(r, g, b);
  
  w = random(10, 40);
  p.setWidth(w);
  p.setHeight(w);
  
  // This is how we modify elements in the array
  if (shiftPressed) {
    // Check to see that the array length is larger than 0
    if (pArray.length > 0) {
      // This will always remove the last element in the array
      pArray = (Particle[]) shorten(pArray); 
    }
  } else {
    // This adds an element to the end of the array
    pArray = (Particle[]) append(pArray, p);
  }
}


void keyPressed() {
  if (key == CODED) {
    if (keyCode == SHIFT) {
      shiftPressed = true;  
    }
  }
}

// This is a function that we haven't talked about
// that is called every time a key is released
void keyReleased() {
  if (key == CODED) {
    if (keyCode == SHIFT) {
      shiftPressed = false;  
    }
  }
}