// Keep track of whether or not the screen needs to be updated
boolean dirty = true;
boolean win = false;
boolean lose = false;

int totalScore = 0;

int scoreIncrement = 10;

Sprite[] spriteArray;

PFont sourceSansPro;
PFont sourceSansProBlack;

int startingSprites = 10;
int maxSprites = 50;
float spriteFadeIn = 5.0; // time in seconds to fade in

float randomSpawnInterval = 0.0;
float spawnFrames = 0.0;

void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
  
  sourceSansPro = loadFont("SourceSansPro-Regular-72.vlw");
  sourceSansProBlack = loadFont("SourceSansPro-Black-72.vlw");
  
  randomSpawnInterval = random(0.5, 1.0);
  
  setStartingSprites();
}

void draw() {
  background(0);
  
  for (int i = 0; i < spriteArray.length; i++) {
    spriteArray[i].display();
  }
  
  checkSpawnFrames();
  
  fill(255);
  stroke(255);
  textFont(sourceSansPro, 32);
  textAlign(LEFT, LEFT);
  text("Current score: " + totalScore, 10, height - 40);
  
  if (spriteArray.length == 0) {
    textFont(sourceSansProBlack, 72);
    textAlign(CENTER, CENTER);
    text("YOU\nWIN!!!\n:-)", width/2, height/2);
    noLoop();
    win = true;
  } else if (spriteArray.length > maxSprites) {
    fill(255, 0, 0);
    textFont(sourceSansProBlack, 72);
    textAlign(CENTER, CENTER);
    text("YOU\nLOSE\n:-(", width/2, height/2);
    noLoop();
    lose = true;
  }
}

void updateScore() {
  totalScore += scoreIncrement;
  
  println("Total score now ", totalScore);
}

void checkSpawnFrames() {
  spawnFrames += 1;
  
  if (spawnFrames >= (randomSpawnInterval * frameRate)) {
    spawnFrames = 0;
    randomSpawnInterval = random(1.0, 1.5);
    Sprite s = new Sprite(random(0, width), random(0, height));
    s.setSpriteFadeIn(2.0);
    spriteArray = (Sprite[]) append(spriteArray, s);
    println("Spawing new Sprite");
  }
}

void mousePressed() {
  
  for (int i = 0; i < spriteArray.length; i++) {
    if (spriteArray[i].checkBoundingBox(mouseX, mouseY)) {
      Sprite[] s1 = (Sprite[]) subset(spriteArray, 0, i);
      Sprite[] s2 = (Sprite[]) subset(spriteArray, i + 1, spriteArray.length - i - 1);
      spriteArray = (Sprite[]) concat(s1, s2);
         
      //spriteArray[i].respawn(); 
      updateScore();
    }
  }
}

void keyPressed() {
  if (key == 'r') {
    if (win || lose) {
      win = false;
      lose = false;
      totalScore = 0;
      loop();
      setStartingSprites();
      background(0);
      println("Resetting game");
    }
  }
}

void setStartingSprites() {
  spriteArray = new Sprite[startingSprites];
  
  for (int i = 0; i < startingSprites; i++) {
    spriteArray[i] = new Sprite(random(0, width), random(0, height));
    spriteArray[i].setSpriteFadeIn(2.0);
  }  
}
