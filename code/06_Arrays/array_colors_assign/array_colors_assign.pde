// Declare our arrays
float[] fillR;
float[] fillG;
float[] fillB;

int numEllipses = 10;

void setup() {
  size(600, 600);
  frameRate(30);
  smooth();

  // And here we do our further array declaration
  fillR = new float[numEllipses];
  fillG = new float[numEllipses];
  fillB = new float[numEllipses];
  
  // Access each array element by its index
  fillR[0] = random(0, 255);
  fillR[1] = random(0, 255);
  fillR[2] = random(0, 255);
  fillR[3] = random(0, 255);
  fillR[4] = random(0, 255);
  fillR[5] = random(0, 255);
  fillR[6] = random(0, 255);
  fillR[7] = random(0, 255);
  fillR[8] = random(0, 255);
  fillR[9] = random(0, 255);

}

void draw() {
  if ((frameCount % 30) == 0) {
    // Print out the value at index 0
    println("fillR[0] is ", fillR[0]);
    
    // Print out the value at index 7
    println("fillR[7] is ", fillR[7]);
  }
}