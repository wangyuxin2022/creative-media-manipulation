int xPos = 0;

void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  
  // Draw a circle at xPos
  ellipse(xPos, height/2, 10, 10);
  
  // Increpent xPos at every frame
  xPos += 1;
}