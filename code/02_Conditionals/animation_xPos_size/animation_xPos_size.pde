int xPosition = 0;
int size = 10;


void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
   stroke(255);
   fill(255);
   background(0);
   ellipse(xPosition, height/2, size, size);
   
   xPosition += 1; // xPosition = xPosition + 1;
   size += 1;
   
   if (xPosition >= width) {
     xPosition = 0; 
     size = 10;
   }
   
   println(frameCount);
}
