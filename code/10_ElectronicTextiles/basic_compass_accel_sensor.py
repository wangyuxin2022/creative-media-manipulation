# Adafruit CircuitPython imports
from digitalio import DigitalInOut, Direction, Pull
from analogio import AnalogIn, AnalogOut
import microcontroller
import board
import neopixel
import busio
import adafruit_lsm303
import neopixel
import simpleio

# Standard library imports
import time

# Setup the neopixel
# We're going to use pin A0, we only have 1 neopixel attached to this pin, and we want the brightness to be somewhat faint
pixels = neopixel.NeoPixel(board.A0, 1, brightness=0.2)

i2c = busio.I2C(board.SCL, board.SDA)
sensor = adafruit_lsm303.LSM303(i2c)

while True:
    accel_x, accel_y, accel_z = sensor.acceleration
    mag_x, mag_y, mag_z = sensor.magnetic
    print("%f %f %f" % (accel_x, accel_y, accel_z))
    
    # Map our accel range to a range more suited to RGB values
    rValue = int(simpleio.map_range(accel_x, -7, 8.5, 0, 255))
    gValue = int(simpleio.map_range(accel_y, -7, 8.5, 0, 255))
    bValue = int(simpleio.map_range(accel_z, -7, 8.5, 0, 255))
    
    pixels.fill((rValue, gValue, bValue))
    pixels.show()
    
    time.sleep(0.5)
