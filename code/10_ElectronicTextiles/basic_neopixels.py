# Adafruit CircuitPython imports
import microcontroller
import board
import neopixel

# Standard Python library imports
import time

# Setup the neopixel
# We're going to use pin A0, we only have 1 neopixel attached to this pin, and we want the brightness to be somewhat faint
pixels = neopixel.NeoPixel(board.A0, 1, brightness=0.2)

while True:
    # Fill the neopixels with a white color
    # Note that we are passing a `tuple`, one of the types of lists that we didn't talk about.
    # Basically a `tuple` is an *immutable* list, meaning that it can't be changed once it is created.
    # Sometimes functions or classes expect `tuple`s to be passed, or return them as return values.
    # A `tuple` works a lot like a list, except elements can't be added once it has been created (i.e., it's immutable!)
    # You enclose your values between pairs of *parentheses*, rather than square brackets like with a `list`.
    pixels.fill((255, 255, 255))
    # Show the pixels (i.e., send the commands to change the color to be white)
    pixels.show()
    # Sleep for half a second
    time.sleep(0.5)
    
    pixels.fill((255, 0, 0))
    pixels.show()
    time.sleep(0.5)
    
    pixels.fill((0, 255, 0))
    pixels.show()
    time.sleep(0.5)
    
    pixels.fill((0, 0, 255))
    pixels.show()
    time.sleep(0.5)
