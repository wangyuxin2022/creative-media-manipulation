from digitalio import DigitalInOut, Direction, Pull
import microcontroller
import board
import time

# Pin to connect to the motor
motor = DigitalInOut(board.D1)
motor.direction = Direction.OUTPUT

while True:
    # You definitely don't want to be powering the motor
    # from the pin continuously. So here we just pulse
    # the motor for a short period of time by setting
    # the output pin, D1, to be HIGH (True, 3.3V)
    # for 0.2 seconds, and then setting it to be LOW
    # (False, 0V) for 1.5 seconds. This is repeated
    # forever.
    motor.value = True
    time.sleep(0.2)
    
    motor.value = False
    time.sleep(1.5)