"""
Functions and classes work very similar to Processing, but the syntax is a bit different.
"""

import Computer

def hello_world():
    print("Hello world!")

def hello_world_name(name):
    print("Hello, %s" % name)

def hello_world_default(name = "Phoebe"):
    print("Hello, %s" % name)

def mult(x, y):
    val = x * y
    return val

def newmult(x, y, z = 10):
    val = x * y * z
    return val

if __name__ == "__main__":
    hello_world()

    hello_world_name("Genevieve")

    hello_world_default()

    hello_world_default("friend")

    val = mult(2, 4.5768493029485)
    print(val)

    val = newmult(2, 3)
    print(val)

    val = newmult(2, 3, 5)
    print(val)

    computers = [Computer.Computer(name = "redstar", computerType ="Mac", manufacturer = "Apple"),
                 Computer.Computer(name = "starfields", computerType = "PC", memory = 4096),
                 Computer.Computer(name = "wire", computerType = "PC", manufacturer = "Dell"),
                 Computer.Computer()]

    print("\nWhat sorts of computers are there?\n")
    for computer in computers:
        print("Your computer is called %s." % (computer.getName()))
        print(computer.printKind())
        print("\n")
