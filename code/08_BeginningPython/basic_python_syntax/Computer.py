"""
This module defines a single class, `Computer`, that's used to explain
how classes work in Python.
"""


class Computer(object):
    """This is a toy class for defining a computer."""

    name = ""
    computerType = ""
    manufacturer = ""
    speed = ""
    memory = 1024

    def __init__(self, name = "Computer", computerType = "PC",
                 manufacturer = "Lenovo", speed = "fast", memory = 2048):
        """Initialize our class."""
        self.setName(name)
        self.setComputerType(computerType)
        self.setManufacturer(manufacturer)
        self.setSpeed(speed)
        self.setMemory(memory)

    def setName(self, name):
        """Set the name of the computer."""
        self.name = name

    def setComputerType(self, computerType):
        """Set the computer's type."""
        self.computerType = computerType

    def setManufacturer(self, manufacturer):
        """Set the computer's manufacturer."""
        self.manufacturer = manufacturer

    def setSpeed(self, speed):
        """Set the computer's speed."""
        self.speed = speed

    def setMemory(self, memory):
        """Set the amount of memory the computer has (in megabytes)."""
        self.memory = memory

    def getName(self):
        """Get the computer's name."""
        return self.name

    def getComputerType(self):
        """Get the computer's type."""
        return self.computerType

    def getManufacturer(self):
        """Get the computer's manufacturer."""
        return self.manufacturer

    def getSpeed(self):
        """Get the computer's speed."""
        return self.speed

    def getMemory(self):
        """Get the computer's memory (in megabytes)."""
        return self.memory

    def printKind(self):
        """Print out what kind of computer it is."""
        kind = ""
        if (self.getComputerType() == "PC"):
            kind = "You have a PC, it probably runs Windows or Linux."
        elif (self.getComputerType() == "Mac"):
            kind = "You have a Mac, it probably runs OS X."
        else:
            kind = "I don't know what you have!"

        return kind