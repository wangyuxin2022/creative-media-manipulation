// Image from https://www.nasa.gov/mission_pages/chandra/galaxy-pictor-a.html

// Processing provides a `PImage` class for working with images
PImage img;

void setup() {
  size(500, 500);
  
  // Load the image from a file
  // This image needs to exist in the "data" folder
  img = loadImage("pictora_500x500.jpg");
}

void draw() {
  // Display the `PImage` starting from the upper left corner
  tint(255, 255, 255);
  image(img, 0, 0);
  
  tint(255, 0, 0, 127);
  image(img, width/2, 0);
}
