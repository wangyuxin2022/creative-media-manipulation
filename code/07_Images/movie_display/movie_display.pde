// You use the same library for reading movie files as you do
// for reading from cameras
import processing.video.*;

// Instead of `Capture`, we instantiate an object of type `Movie`
Movie movie;

void setup() {
  // This is the size of the movie file
  // You'll probably need to crop and resize a movie file using
  // other programs
  size(500, 500);
  
  // Like `Capture`, you need to use the `this` keyword, along with the
  // name of the movie file you want to load.
  // See the Processing reference for what kinds of files `Movie`
  // can read.
  movie = new Movie(this, "waving_500x500.mov");
  
  // You need to tell the movie object to start playing.
  // `play()` will cause the movie to play through once,
  // while `loop()` will loop it forever.
  movie.play();
  // movie.loop();
}

// Instead of `captureEvent` you have `movieEvent`, which is called
// every time there is a new frame to be read
void movieEvent(Movie m) {
  m.read(); 
}

void draw() {
  // You display each frame of the movie like you do with reading
  // frames from a camera
  image(movie, 0, 0);  
}