// Create an array of PImages
PImage[] frames;

// Set the number of frames
int numFrames = 10;

// Set a counter to keep track of what image we're on
int counter = 0;

// Set a boolean to know whether to decrease the counter
boolean decrease = false;

void setup() {
  size(1000, 900);
  
  // Create an array of images
  frames = new PImage[numFrames];
  
  // Load all of our frames
  println("Loading images...");
  for (int i = 0; i < numFrames; i++) {
    println("Loading image " + i);
    // This allows us to load each image programatically!
    frames[i] = loadImage("animation_frames_00" + i + ".png");  
  }
  println("Images loaded");
}

void draw() {
  background(255);

  // Display the specified image
  image(frames[counter], 0, 0);
  
  if ((frameCount % 2) == 0) {
    if (decrease) {
      counter -= 1;
    } else {
      counter += 1;
    }
  }
  
  // Note the numFrames - 2 to take into account zero indexing
  if (counter > (numFrames - 2)) {
    decrease = true;  
  } else if (counter == 0) {
    decrease = false;  
  }
}