// Import the video library
import processing.video.*;

// Create an object for working with the camera's video
Capture video;

void setup() {
  // Set a window size that is appropriate for capture
  size(640, 480);

  printArray(Capture.list());

  // Create a new `Capture` object
  // We first have to provide a reference to the current sketch
  // which we do through the `this` keyword
  // We then define the size of the Capture to use.
  // This needs to be a resolution that your camera supports.
  video = new Capture(this, width, height);
  
  // We then start the capture
  video.start();
}

// We only read from the camera when there is a new frame ready.
// Since we can have multiple cameras attached to the computer,
// we have to pass a specific `Capture` object to this function.
void captureEvent(Capture video) {
  // Actually read from the camera
  video.read();  
}

void draw() {
  // The `Capture` object functions in many ways like a `PImage`,
  // so you can just use it in `image()` like usual.
  image(video, 0, 0);
}