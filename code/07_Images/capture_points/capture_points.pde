import processing.video.*;

int scale = 4;
int cols, rows;
Capture video;

void setup() {
  size(1280, 720);

  printArray(Capture.list());
  video = new Capture(this, width, height);
  video.start();
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  video.loadPixels();
  
  for (int i = 0; i < 10; i++) {
    int x = int(random(0, width));
    int y = int(random(0, height));
    int loc = (video.width - x - 1) + y * video.width;
    color c = video.pixels[loc];
    float sz = random(10, 25);
    
    fill(c);
    noStroke();
    ellipse(x, y, sz, sz); 
  }
      
}