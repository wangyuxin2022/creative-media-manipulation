import processing.video.*;

Capture video;
int prevx, prevy;

void setup() {
  size(1280, 720);
  
  video = new Capture(this, width, height);
  video.start();
  
  prevx = int(random(0, width));
  prevy = int(random(0, height));
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  video.loadPixels();
  int x = prevx;
  int y = prevy;
  
  for (int i = 0; i < 50; i++) {
    x += int(random(-20, 20));
    y += int(random(-20, 20));
    
    if (x >= width) {
      x = width - 1;  
    } else if (x < 0) {
      x = 0;  
    }
    
    if (y >= height) {
      y = height - 1;  
    } else if (y < 0) {
      y = 0;  
    }
    
    int loc = x + y * video.width;
    color c = video.pixels[loc];
   
    stroke(c);
    strokeWeight(5);
    line(prevx, prevy, x, y);
    prevx = x;
    prevy = y;
  }
      
}