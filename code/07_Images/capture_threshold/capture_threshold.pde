import processing.video.*;

Capture video;

// Like the still image version, we need a destination
// image and threshold value
PImage destinationImg;
float threshold = 127;

void setup() {
  size(640, 480);

  // This is just like the still image example
  destinationImg = createImage(width, height, RGB);

  video = new Capture(this, width, height);
  video.start();
}

// Read from the camera when ready
void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  // Local variables to store the brightness and color
  // of the given pixel
  float b;
  color c;
  
  // Since the `Capture` object functions like a `PImage`,
  // we can just loadPixels() like before
  video.loadPixels();
  destinationImg.loadPixels();
  
  // This loop is exactly like the still image example,
  // we just use `video` instead of a source still image.
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      int loc = i + j * video.width;
      
      c = video.pixels[loc];
  
      b = brightness(c);
      
      if (b > threshold) {
        destinationImg.pixels[loc] = color(255, 255, 255);        
      } else {
        destinationImg.pixels[loc] = color(0, 0, 0);  
      }
      
    }
  }
  
  // And the update and display is just like before
  destinationImg.updatePixels();
  image(destinationImg, 0, 0);
}

// This is exactly the same as before
void mouseMoved() {
  threshold = map(mouseX, 0, width, 0, 255);  
}