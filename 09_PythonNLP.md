# Basic natural language processing (NLP), or how to generate text from a model

<!-- python, nltk, tokenizing, bigrams, trigrams, conditional frequency distribution, generating text from scratch, hapaxes -->

## Natural Language Processing



## Poetry generation

Setting up combinations of phrases that, when juxtaposed in random combinations, produce interesting/engaging/thought-provoking/unexpectedly-poignant/surprising/confusing/understandable/profound output. 

## 

![](media/KnowlesHouseOfDust_1500px.png)

##

See online implementation by Nick Montfort: https://nickm.com/memslam/a_house_of_dust.html

##

```python
openings = ["It was morning.",
            "It was afternoon.",
            "It was raining.",
            "We left the barn at daybreak.",
            "We went.",
            "They saw us there."]
doings = ["Running",
          "Walking",
          "Waiting",
          "Loitering",
          "Sleeping",
          "Dozing",
          "Thinking",
          "Dreaming",
          "Loving",
          "Wanting"]
```

##

See Montfort's (and collaborators) *2x6*: https://nickm.com/2/

## Natural Language Processing

Using a computer to try and *understand* language:

* Components of a sentence or a paragraph; *tokens*
* Parts-of-speech (nouns, verbs, adjectives, adverbs, articles, etc.)
* How a sentence is put together
* *Sentiment analysis*, determining whether a sentence is biased or not, positive or negative in valence
* Word frequencies; uniqueness of a text

What could be some of the challenges of these goals?

## A somewhat-challenging sentence

"The computer will refuse to understand the refuse of society."

##

![Tagged sentence.](media/POSTagging.png)

## `nltk`

[Natural Language Toolkit](https://www.nltk.org/)

Most complete text processing library for Python

See the [nltk book](http://www.nltk.org/book/) for some tutorials (unfortunately written at a pretty advanced linguistic level)

## Downloading and setting up `nltk`

![Installing a package.](media/InstallPackage.png)

##

![Installing `nltk`.](media/InstallPackage_nltk.png){ width=75% }

## `nltk.download()`

![`nltk.download()`.](media/nltk_download.png){ width=50% }

## Tokenizing text

```python
import nltk
words = nltk.word_tokenize(some_string)
sentences = nltk.sent_tokenize(some_string)
```

## Parts of speech tagging

```python
words = nltk.word_tokenize(some_string)
tagged = nltk.pos_tag(words)
```

## Frequency Distribution

```python
freqdist = nltk.FreqDist(words)
freqdist.most_common(50)
```

## Corpus, corpra

corpus

: a collection of textual material gathered into a unit for analysis

## Brown corpus

```python
from nltk.corpus import brown
words = brown.words()
```

Many, many other corpra to download and use

## Basic sentiment analysis

"VADER (Valence Aware Dictionary and sEntiment Reasoner) is a lexicon and rule-based sentiment analysis tool that is *specifically attuned to sentiments expressed in social media*."

See original VADER code at: https://github.com/cjhutto/vaderSentiment

Be sure to use `nltk.download()` to download `vader_lexicon`.

##

```python
from nltk.sentiment.vader import SentimentIntensityAnalyzer

sid = SentimentIntensityAnalyzer()

sid.polarity_scores("I really love what you've done with your hair!")
```

Where could this be useful? 

What could go wrong?


## Bigrams

bigrams

: the set of all consecutive pairs of tokens in a corpus 

##

"The computer will refuse to understand the refuse of society."

```python
bigrams = ["The computer", "computer will", "will refuse", "refuse to", "to understand", "understand the", "the refuse", "refuse of", "of society", "society."]
```

## Trigrams, NGrams

What would a *trigram* be? An *NGram*?

## Conditional frequency distribution (cfd)

conditional frequency distribution (in NLP)

: The number of times a particular word appears in a corpus, *given* a particular previous word

## cfd for our sentence

the: computer, refuse

computer: will

will: refuse

refuse: to, of

to: understand

understand: the

of: society

cfds are especially useful in large corpra

## Looking at some cfds for a text

cfd for *Alice in Wonderland*

## Choosing an element randomly

```python
import random
l = ["one", "that", "whose", "planet", "journey", often"]
random_string = random.choice(l)
```

## Generating text based on a cfd

```python
def generate_text(cfd, start_word, num = 20):
    word = start_word
    all_words = "%s" % word

    for i in range(0, num):
        new_word = random.choice(list(cfd[word].keys()))
        all_words = "%s %s" % (all_words, new_word)
        word = new_word

    return all_words
```

## Twitter corpus

Be sure to download `twitter_samples`

```python
from nltk.corpus import twitter_samples
tweets = twitter_samples.strings()
```

## More advanced: accessing tweets

See tutorial at: http://www.nltk.org/howto/twitter.html

```python
from nltk.twitter import Twitter
tw = Twitter()
tw.tweets(keywords="love hate", limit=10, stream=False)
```

## Exercise

Find an interesting series of texts online (collected from webpages, news sites, Facebook, Project Gutenberg, etc.). Combine them into one large text file and create a *model* from it that calculates the bigrams and conditional frequency distribution. Generate a paragraph or two of material from it (you might need to do this many times until you find something interesting). Run sentiment analysis on your generated sentences/phrases to see what the computer thinks of them.
