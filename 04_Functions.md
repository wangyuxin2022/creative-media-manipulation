# Functions, or learning how to make your programs modular

<!-- functions, void, arguments, passing by value, return values, multiple types -->

## It's like a recipe, but not exactly...

If you recall from one of the first class meetings, we talked about some of the main ideas of programming that we're exploring this semester: *abstraction*, *modularity*, and *representation*. We've spent a fair bit of time on representation, in the context of variables and control flow, and we've touched on abstraction in our discussion of loops. Today we're going to turn to *modularity*, which is accomplished in programming through the idea of the *function*.

Like with variables, there are a lot of metaphors that are often used to talk about programming and functions in particular. The recipe is one of the main ones you'll come across. But like before, I think the metaphors get in the way of understanding the actual fundamentals. So here's a definition of a function:

function

: A repeatable fragment of code, whose behavior can optionally change depending on its arguments, and can optionally return a value

We're going to to through each of these elements one by one.

## The most simple function

Let's say that we'd really like to print "Hello, world" to the console quite often in our code. We could just write `println("Hello, world");`, but our professor really wants us to use functions to learn their syntax. So this is how we'd write a function to do just that.

```java
void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  if ((frameCount % 30) == 0) {
    hello_world();
  }
}

// This is our function that we're defining
// Take note of the syntax
void hello_world() {
  println("Hello, world: ", frameCount);
}
```

The syntax of the function should be familiar to you, as we've been writing the code for functions since the very first day, they've just been the *special* functions of `setup()` and `draw()` (and others).

In this case, we've defined a function as returning `void` (which means it doesn't return a value; we'll turn to this in a bit so for the moment, just take it upon faith) and named `hello_world`. The naming requirements for functions are the same as for variables. You need two parentheses, open and close, since this function doesn't take any arguments (again, take this on faith for right now). Then we use curly brackets to open up a new block of code, and then we write our desired code in the function. The amount of code in the function can be arbitrarily large. Let's see another example.

```java
void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  if ((frameCount % 30) == 0) {
    hello_world();
  }
}

// This is our function that we're defining
// Take note of the syntax
void hello_world() {
  println("Hello, world: ", frameCount);
}

// This is a function that outputs the mouse coordinates when the button is clicked
void print_mouse() {
  int x = mouseX;
  int y = mouseY;

  println("Mouse was clicked at: ", x, " ", y);
}

void mousePressed() {
  // Here we call our defined function
  print_mouse();
}
```

We've written a new function `print_mouse()`, that takes the current `x` and `y` coordinates of the mouse and outputs them to the console. Note the use of local variables! Then, in `mousePressed()`, we're able to *call* this function. To "call" a function is to use it in your code.

The functions we've defined so far are useful if you just have a fragment of code that you'd like to be able to repeat. Perhaps you do the same thing over and over in your code, and you want to only write this code once. You'd use a function to make your code more *modular*, so that you only have to potentially make changes in *one* place in your code, rather than in many places. Recall that we had similar reasoning for the use of loops last week.

But functions can do so much more than just repeat code. They can also do things that *vary* depending on their *argument*. Let's turn to a further elaboration of functions.

## Letting our functions vary

Imagine that the designers of Processing only allowed a certain set of sizes for your window. And imagine that they were called something like the following:

```java
size_600_600();
size_300_400();
size_200_200();
size_100_500();
// and so on
```

If the designers of Processing had done this, then they would have had to imagine *all* of the possible sizes of window when they wrote the program. If someone had wanted to create a window that was 1024 pixels by 768 pixels, then you had better hope that the programmers had thought of that when they designed Processing.

Thankfully the designers decided to *modularize* their code, and allow two *arguments* to the `size()` function they created. That allows us to use *whatever* size we'd like, depending on our whim or the size of the window or the assignment parameters. Let's write a definition of an argument:

argument

: A value passed to a function to allow its behavior to vary

The arguments to `size()` allow us to define the `x` and `y` sizes of our window. We can also write functions with arguments. Let's look at an example:

```java
void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
}

// This is a function that takes x and y coordinate values as input
// We have to declare the type of the variables that we're going to use
// as input
void print_mouse(int x, int y) {
  println("Mouse was clicked at: ", x, " ", y);
}

void mousePressed() {
  // Here we call our defined function
  // and pass the variables that we'd like the function to use
  print_mouse(mouseX, mouseY);
}
```

This code does the same thing as before, we've just written it to use arguments. Take a look at the new definition for `print_mouse()`. We've now added two *arguments*, `int x` and `int y`. We declare these variables like we've gotten used to. But we don't assign a value to these variables. Why not? Because the value of the variables is *undefined* until the function is actually called. What we're doing here instead is letting Processing know that `print_mouse()` expects two `ints` as input. We then use whatever values were *passed* to Processing in those two arguments, and we refer to those two arguments*within* the function as `x` and `y`.

Now, those arguments are then **local** to the function itself. I'll repeat myself: the arguments are **local** to the function itself. This is a very important point to understand. And it means that we will have to talk a bit about what is a rather complicated topic in just a moment. But let's go to another example first.

```java
void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  int x = 0;
  int y = 0;
  
  x = mouseX;
  y = mouseY;
  
  // Call our function every second
  if ((frameCount % 30) == 0) {
    multiply_mouse(x, y);
    println("The value of x and y is: ", x, " ", y);
  }
}

// This is a function that takes x and y coordinate values as input
// We have to declare the type of the variables that we're going to use
// as input
void multiply_mouse(int x, int y) {
  // This might look wonky, but it's perfectly
  // acceptable Processing code
  // We're assigning to x, which was already
  // defined as an argument,
  // the result of multiplying x by x.
  // When you're reading assignment expressions like this,
  // read them from right to left if they look confusing.
  // And think of "=" as "assign to"
  x = x * x;
  y = y * y;
  println("Mouse coordinates squared: ", x, " ", y);
}
```

Let's take a look at this in depth. Start at the end of the code. We're defining a new function `multiply_mouse()`. It takes two `ints` as input. Since those variables have already been defined (in the argument declaration of the function), we can *assign* new values to them, including values that involve multiplying the variable by itself. We then output the squared variables to the console.

Turn to the `draw()` method now. Every frame we save the value of the mouse coordinates into `x` and `y` variables. Every 30 frames (or very second) `multiply_mouse()` is called with those `x` and `y` variables, and then the values of those `x` and `y` variables are outputted to the console. Yet we see that the output of `multiply_mouse()` and the `draw()` method are different. Why is that?

Input values are **LOCAL** to a function.


### The saga of variables, or passing values not references

One could image that the behavior would have been different. We could expect that when we pass a variable to a function, and then do something to that function, then that *something* would affect the argument itself. That would involve something called *pass by reference*, which is **not** how Processing works. Other programming languages do allow this, but for now, just put this idea out of your head!

Processing passes by *value*. Imagine that Processing creates a new copy of the variable whenever it is passed to a function. The function works on that copy, and that copy alone. It doesn't change the original. If you want to change the original value, you have to write your function so that it *returns* a value. 

We'll turn to that concept in just a bit. But first, let's look at a more complicated example with multiple functions and arguments. Be sure to read this example closely, paying specific attention to the comments.

```java
// This sketch moves an ellipse randomly around the window
// Global variables
int ellipseX = 0;
int ellipseY = 0;
int speed = 1;

void setup() {
  size(600, 600);
  // We initialize the starting point of the ellipse
  // after we've set the size, and we set it to the
  // middle of the window, ensuring that the value
  // is an int
  ellipseX = int(width/2);
  ellipseY = int(height/2);
  background(0);
  frameRate(30);
}

void draw() {
  // Clear the background each frame
  background(0);
  
  // Update the speed every second
  if ((frameCount % 30) == 0) {
    update_speed();
  }
  
  // Update the X or Y position approximately
  // half of the time, respectively
  if (random(0, 1) >= 0.5) {
    update_positionX();  
  } else {
    update_positionY();
  }
  
  // Actually draw the ellipse
  draw_ellipse(20, 20);
}

// Choose a random speed, either positive or negative
// Ensure that it is an int
void update_speed() {
  speed = int(random(-7, 7));  
}

// Update the X coordinate of our ellipse
void update_positionX() {
  ellipseX = ellipseX + speed;
  
  // Ensure that the ellipse stays within the bounds
  // of the window
  if (ellipseX < 0) {
    ellipseX = 0;
  } else if (ellipseX > width) {
    ellipseX = width;
  }
}

// Do the same for the Y position
void update_positionY() {
  ellipseY = ellipseY + speed;
  
  if (ellipseY < 0) {
    ellipseY = 0;
  } else if (ellipseY > height) {
    ellipseY = height;
  }
}

// Draw our ellipse
// Note that here we can control the fill and stroke
// without worrying about what it is elsewhere
// in our code
void draw_ellipse(int w, int h) {
  fill(255);
  stroke(255);
  ellipse(ellipseX, ellipseY, w, h);
}
```

Things to note:

* Global variables are used so that we can easily manipulate the speed and `x` and `y` positions of the ellipse from within different functions.
* We can more easily know what color the ellipse will be drawn given that we can *specifically* define the stroke and fill from within a separate function.
* We use conditionals to keep the ellipse from moving off screen.
* We can can use the `random()` function, along with type casting, to change the speed at a regular rate in our code.

Now it's time to talk about return values.

## There's more to functions than `void`...

So far we've always defined our functions beginning with the word `void`. This means that the function does not return a value. So, what do we mean by a *return value*?

return value

: A value that is returned to the function's caller

That definition probably doesn't make much sense. Let's take a basic example.

```java
void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  int squareX = 0;
  int squareY = 0;
 
  // Take the mouse coordinates, square them, and assign
  // them to our local variables
  squareX = square_value(mouseX);
  squareY = square_value(mouseY);
  
  if ((frameCount % 30) == 0) {
    println("Squared mouse coordinates: ", squareX, " ", squareY);  
  }

}

// This function returns a value, an int
int square_value(int x) {
  return (x * x);  
}
```

Note in particular the definition of `square_value()`. We've defined it not with `void` but with `int` to let Processing know that the function will return a value, an `int`. We can then use that function in an *expression*, assigning it to a variable.

This is how functions like `random()` or `sin()` work. They *return* a value, in both cases a float. And you could define a version of `square_value()` that would work on floats too...

### Petitioning for an overload

Let's say that you want to write code to square `float`s. You can't use the version of `square_value()` that we just wrote, as that only works for `int`s. You could write something called `square_value_float()`, but that's more verbose than you'd like. Is there another way?

In Processing there is, and it uses the idea of *overloading* a function. We can use the same function name, but with different parameter types (or different *numbers* of parameters[^stroke]). Let's see an example.

[^stroke]: This is how the different versions of functions like `stroke()` work. It's overloaded such that when it's called with one parameter, it sets all R, G, B, values to be the same (thus greyscale), but if it's called with three parameters then the R, G, B, values are set to those values respectively.

```java
void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  int squareX = 0;
  int squareY = 0;

  squareX = square_value(mouseX);
  squareY = square_value(mouseY);

  if ((frameCount % 30) == 0) {
    println("Squared mouse coordinates: ", squareX, " ", squareY);
  }

  if ((frameCount % 60) == 0) {
    float randomValue = random(100, 200);
    // Note how I'm calling the function here within the println statement
    // And it is exactly the same name as above, but only called with a float
    println("Squared float value: ", square_value(randomValue));
  }
}

// This function returns a value, an int
int square_value(int x) {
  return (x * x);
}

// This function returns a value, a float
// It's an overloaded version of the previous function
float square_value(float x) {
  return (x * x);
}
```

Note how we create a new function with the *same name* but *different return type and argument(s)*. We can then call it in our `println()` statement.

Overloading functions allow you to write *conceptually* cohesive code. While Processing cares about whether or not the variables are `float`s or `int`s, it shouldn't matter so much to you as the programmer, or to others who might be using your code. So overloading allows you to write code that manipulates *data* rather than *variables*. This is a subtle, conceptual distinction. It's one where you begin to think of your code not at the level of variable declarations, but a bit higher at the level level of concepts and data processing. This is a new level of *abstraction* that we'll be delving into in greater detail when we start to talk about *objects*.

## Writing equations as functions

Let's go back to our idea of parametric equations from a couple of weeks ago, and write them using functions. Consider the following parametric equations:

```java
x(t) = 154 * sin(t*0.09) - 100 * cos(t*0.02);
y(t) = 50 * sin(t*0.03) + 200 * cos(t*0.07);
```

> How could you use functions to evaluate this parametric equation?

And then,

> Draw continuously, using ellipses, the results of this equation at `t = 0` and for ten steps into the future.

## Universal text

In dealing with keyboard input we've only checked for lower-case characters such as 'e' or 'm'. That's because we haven't dealt with *unprintable* characters such as special keys like "shift" or "return" or "up arrow". Unprintable characters, you say? Yes, there are such things, and we're going to go down a bit of a rabbit hole here....

Remember how I mentioned earlier that the question of exactly *how* to represent letters in a computer is a really challenging task? It's challenging because fundamentally computers don't deal with letters, of course, they deal with numbers. And so there has to be some standardization with how you create the *mapping* between some number and how it should be represented on-screen. In the past, when computing memory and processing speed were limited, there were often only 7 bits, or 127 values, that could be used to represent all of the characters you need. Now that seems a lot, if your worldview is limited to western, or "Latin" languages. But of course there are languages that use *a lot more* characters than just 127, and we'll get to that in a moment.

So we have 127 possible values. Once you add in upper case and lower case letters (in English) and punctuation, you only have a few tens of characters remaining. And those characters are called *control characters*, and they were the "unprintable" characters designed to control output, like a printer or screen. In fact, the idea of a "return" key comes from the days of using a typewriter, where you would press return to literally *return* the printing head to the beginning of the next line, where you could start typing again.

This system of 7 bits was called ASCII, for American Standard Code for Information Interchange. As the title suggests, it was rather geographically limited, but was widely implemented. In ASCII, the letter 'e' is represented by the number 101 (0x65), the letter 'f' by 102 (0x66), and so on. So when, in `keyPressed()`, you're checking to see if `key` is equal to 'e', you're actually asking whether `key == 101`. 

Of course limiting your possible characters to English letters is no good. So a whole variety of *code pages* developed around the world from the 1970s until the 1990s. Extended ASCII is one of those code pages that extended ASCII to 8 bits, allowing for 256 possibilities and covering most of the languages of Western Europe. You can see a table of Extended ASCII and all of the numerical encodings here: https://www.ascii-code.com/ .

Now the solution to all of this was to develop a new encoding system called Unicode that attempts to represent hundreds of contemporary and historical scripts, as well as multitudes of symbols. Sometimes you might see something termed as UTF-8, and that means that the text is encoded in Unicode. Currently Unicode represents 136,755 characters. The full code chart can be found here, but **WARNING**, it's a 106 megabyte PDF file: http://www.unicode.org/Public/UCD/latest/charts/CodeCharts.pdf. It's a fascinating document to look at, though, if you're interested in the variety of writing systems that computers can represent.

While Unicode was supposed to solve all of our textual representation problems, it turns out that things are always more complicated than we'd like. There are still a number of tricky problems to solve, that we won't be able to get into here. And with text encoded with legacy code pages, it's still possible to get text that looks "wonky" because the renderer (the program, the browser, etc) doesn't know which code page to use.

All of this is to say that text is hard!

## Other kinds of key presses

Back to Processing. If Unicode were not difficult enough, we also have to deal with *coded* key presses, such as the up and down arrow keys. To test whether or not an up or down arrow was pressed, you have to first check where the key was coded, and then check to see what the `keyCode` was. Here's an example:

```java
void keyPressed() {
  // Check whether coded key
  if (key == CODED) {
    // Check keyCode
    if (keyCode == UP) {
      println("UP pressed");  
    } else if (keyCode == DOWN) {
      println("DOWN pressed");  
    }
  } else {
    if (key == 'e') {
      println("'e' key pressed");  
    } else if (key == 102) { // can also check ASCII code
      println("'f' key pressed");  
    }
  }
}
```

For more on keys and `keyCode`, see the Processing reference at https://processing.org/reference/key.html and https://processing.org/reference/keyCode.html .

## Assignment

Remember that you will be graded on your programming style as well as the correctness of the code.

Because of the difficulty of the recent assignments, I'm going to lighten this one up a bit. The next couple of weeks have important concepts that we're going to put together into a more complicated assignment in the coming weeks that will tie together everything that you've learned so far.

Also, for this week, I'm going to be a bit more prescriptive than usual. That's because I want to ensure that you understand the idea of functions at a deep level.

In this assignment you're going to learn how to draw *sprites* that you can then add and change at will. Remember back to your very first assignment, where you drew a face with drawing primitives? I'm going to ask you to do something similar here using functions. 

**NB**: In all of our examples so far we've only used `int`s when describing our `x` and `y` coordinates in drawing functions. So, for example, when we use `line()`, we've ensured that the coordinates are all `int`s. It turns out that all of those drawing functions can also take `float`s as input arguments. So from now on, unless otherwise stated, you can use `float`s as input arguments to these drawing methods without having to worry about converting them to `int`s; Processing knows how to deal with non-integer pixel values.

All files should be uploaded to a 04_Functions folder on Sakai. As usual, the assignment is due the next Thursday night by 11:59:59PM.

![Character sprites for *Super Mario Bros* for the Nintendo Entertainment System. Image from <http://www.mariouniverse.com/sprites-nes-smb/>](media/characters.png)

1. Drawing multiple sprites on screen.

* You need a `void` function called `draw_sprite` that takes as its input two `int`s, `x` and `y` that define the center point for your sprite. Now a sprite is basically a repeatable graphic that is often used in animation or gaming. For example, Mario in Super Mario Brothers is a sprite. So your sprite needs to be built up using at least three drawing primitives. The tricky thing in designing your sprite is that you have to draw it based off of the arguments to your function `x` and `y`.
* You need to be able to control the number of sprites on screen at any one time in a `numSprites` variable. Then, in your `draw()` method you'll draw `numSprites` in your window at random positions. **BUT** you don't want to draw them in new places every frame. You only want to draw the sprites once, and then not draw them again until you need to, until the screen becomes *dirty*. This is actually a "technical term" which means that something has changed requiring the screen to be updated. **HINT**: you'll probably want to use a `boolean` here.
* You can change the number of sprites on screen by pressing the up and down arrows. Each press of "up" adds 1 to `numSprites` and then redraws the sprites (in new random positions, because the screen is now dirty), and each press of down subtracts 1 from `numSprites`. **HINT**: you probably want to ensure that `numSprites` is never less than 0.
* Pressing 'r' resets `numSprites` to 0 and redraws the screen.

Call thise code LASTNAME_FISTNAME_Functions_sprites and upload a screenshot as usual.

2. Getting your sprite to follow the mouse

In this part you'll be drawing one sprite on screen at a time, but it'll follow your mouse pointer. To do this we're going to introduce the concept of *easing* and draw on how to calculate Euclidean distance.

Easing refers to the smooth translation (or movement) of one object towards another. The calculation of this is quite easy. Let's say our target has coordinates `(targetX, targetY)`, and your object has coordinates `(objectX, objectY)`. To *ease* your object towards your target, you can calculate the following equations:

```java
// The "d" here refers to difference (or delta), 
// so you can read that as "difference (delta) in x"
dx = targetX - objectX
dy = targetY - objectY
updated_objectX = objectX + easing * dx;
updated_objectY = objectY + easing * dy;
```

You can see an example of this in action here: https://processing.org/examples/easing.html .

Similarly, you can compute the Euclidean distance between two points `(x1, y1)` and `(x2, y2)` using the following equations. This can be derived, of course, but we're not going to do that here...just take it on faith if you don't want to go into the details.

```java
dx = x1 - x1
dy = y1 - y2
distance = sqrt(dx*dx + dy*dy)
```

In this case, `sqrt()` refers to the square root operation (remember, the square root of 4 is 2, the square root of 9 is 3, etc), and is also the name of the function that you can use in Processing to calculate the square root. Consult the Processing reference for more information. For more information about Euclidean distance see https://en.wikipedia.org/wiki/Euclidean_distance .

With these preliminaries out of the way, here's the assignment:

* Draw a sprite on the screen using the function you wrote in the first part of the assignment. This sprite will follow your mouse pointer on the screen. Write a function called `xeasing()` with arguments `targetX`, `objectX`, and `easing`; each of these parameters should be a float. The function should return a float that is the new `objectX` coordinate. Write a similar function for the `y` coordinates. You can play around with different values for `easing`, but a good place to start is 0.05.
* Every ten frames, calculate the distance from the target to the object. You should calculate this distance in a new function called `distance()` with arguments `x1`, `y1`, `x2`, `y2`, all floats. The function should return a float, the distance between those two points. Print this distance to the console.

Name this code LASTNAME_FISTNAME_Functions_easing and upload a screenshot as usual.
