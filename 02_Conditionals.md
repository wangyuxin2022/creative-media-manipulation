# Conditionals, or how to work with logic

<!-- &&, !!, !, <, >, <=, >=, truth tables, Boolean logic, if, else if, else, println, constrain, increase, decrement, movement from left to right, mouse in quadrants -->

## Conditionals, before we turn to logic

This week we turn to *conditionals*, or the ability to do different things based on the values of our variables. You already have experience with the idea of conditionals in your use of language, so we'll spend some time learning how to *translate* our understanding of natural language into computer code. Along the way we'll have to learn a bit about logic, specifically *Boolean logic*, and explore the idea of *truth tables*. We'll also learn more about the Processing language, including how to print out text to the *console*. We'll finish with an assignment that hearkens back to the early days of computer art.

So let's get a definition of *conditional*, in the context of computer programming, out of the way:

conditional

: The ability to perform different actions based on whether or not a statement is true or false

We'll unpack this definition in a bit. But let me prove to you that you already have a basic idea of this concept. Consider the following sentences:

* If it is raining outside, I need to bring my umbrella.
* If I am hungry, I should eat some food.
* If I win the lottery, I don't have to get a job.
* If it is raining or it is sleeting, I should wear my rain boots.
* If I am tired and I am in class, I should try and stay awake.
* If I am tired and it is after 3AM, I should try and sleep.
* If I am tired and it is after midnight and I am in class, I should rethink what school I am attending[^caltech].

[^caltech]: True story: at Caltech there was a section of the introductory computer science course, CS1, that met from 12AM-2AM.

So we have a fair bit of knowledge of how to represent possible change in our behavior based on the condition of other knowledge about the world. And we can thus begin to *represent* this knowledge in our computer programs. But recall our reading of Agre: this is simply a *representation* and should not be taken to correspond exactly with the actual state of the world. 

Before we do this, however, we'll have to turn to some logic.

## Boolean logic

Computers, as we learned last week, function solely on the manipulation of binary numbers, "simple" combinations of zeros and ones. And computers are constructed to deal with the *logical* combinations of these zeros and ones in what is called *Boolean Logic* after the British mathematician George Boole (1815--1864). In the late 1930s, Claude Shannon, before his more well-known research on the mathematical theory of communication, proved that one could use electronic switching systems (like the telephone system, or digital switches) to solve any Boolean logic problem. This insight led to the development of digital computing systems and underlies all modern computers[^masters].

[^masters]: This work of Shannon's was done as his *master's thesis* and it has been called one of the most consequential and influential master's theses of all time.

Boolean logic deals with the results of combinations of true and false statements. It relies on the logical operators "and", "or", and "not", and functions similarly to how we understand those terms in language. To formalize these relationships, though, we use what are called *truth tables*, as described in the next section.

### Truth tables

Truth tables show the outcome of Boolean logic operators based on various inputs. We can write truth tables in terms of `true` and `false`, but in computers `true` and `false` can also be represented in different ways. So a `1` represents `true` and a `0` represents `false`. This convention, which is almost universal amongst computer languages, will become useful in a bit.

Here is the truth table for the logical operator "and", which in Processing is two ampersands in a row, `&&`[^operators]:

  x     y    x && y
----- ----- --------
  0     0      0
  0     1      0
  1     0      0
  1     1      1

Table: Truth table for the logical operator "and"

[^operators]: Other computer languages use different characters for the operators. For example, in Python, you can just use the word `and` or `or`.

Here is the truth table for the logical operator "or", which in Processing is two "pipe" characters in a row, `||`. On a US keyboard the "pipe" or "|" character is typed with `shift-backslash`.

  x     y    x || y
----- ----- --------
  0     0      0
  0     1      1
  1     0      1
  1     1      1

Table: Truth table for the logical operator "or"

And here is the truth table for the logical operator "not", which in Processing is produced using the exclamation point before the variable, `!`. Because negation only applies to a single truth condition, there is only one *operand* in this truth table, `x`, rather than the two found in the previous logical operators.

  x    !x  
----- -----
  0     1 
  1     0

Table: Truth table for the logical operator "not"

Finally, here's the operator "exclusive or", "xor", which in Processing is done using the "^" or "caret" character. While we will not be using this logical operator very much, if at all, in this class, it's an important one to know about if you go further in programming. Its truth table is the following:

  x     y    x ^ y
----- ----- --------
  0     0      0
  0     1      1
  1     0      1
  1     1      0

  Table: Truth table for the logical operator "xor"

### Boolean logic operators in language

So let's think about these operators in terms of language. The operator "and" outputs `true` only when *both* of the inputs are true. The operator "or" outputs `true` when *any* of its inputs are true. And the operator `not` outputs the opposite of its input.

Let's consider some examples in natural language. We're going to base our statements off of the following citation from the syllabus:

> Jennifer S. Light, "When Computers Were Women," *Technology and Culture* 40, no. 3 (1999): 455–483, http://www.jstor.org/stable/25147356 

Are the following statements true or false?

* The author's name is Jennifer S. Light or the author's name is Lisa Nakamura.
* The year the article was published was 1999 and the volume was 40.
* The year the article was published was 1999 and the journal's name was *Critical Inquiry*.
* The website that hosts the article is not http://journals.sagepub.com.
* The article starts on page 435 or the article ends on page 483.
* The article's author is not Jennifer S. Light.

The best way to explore this through computer language is to use examples with real variables, so let's move on to that now.

### Boolean logic with variables

Consider the following variable declarations and the associated statements.

```java
boolean sunShining = false;
boolean raining = true;
boolean snowing = true;
boolean galoshes = false;
boolean result = false;

result = sunShining && galoshes;
result = sunShining || raining;
result = !snowing;
result = ((raining || snowing) && galoshes);
```

We can also consider logical operators with *numbers* if we use comparison operators like "greater than" ("or equal") and "less than" ("or equal"), `>, >=, <, <=`, respectively. Now consider the following variable declarations and statements^[Note that the result on line 8 in the listing is actually `false`! This has to do with the fact that in Processing `float` is not guaranteed to be precise due to rounding errors. If you want to do a comparison like this, you'll need to use the `double` type, but you'll also eventually have to type cast to a `float`. For more information see <https://processing.org/reference/float.html> and <https://processing.org/reference/double.html>.]:

```java
int val1 = 37;
int val2 = 23;
float val3 = 37.0000000000001;
boolean result = false;

result = val1 > val2;
result = val2 < val1;
result = val3 > val1;
result = val3 >= val1;
```

What if you want to tell whether two values are equal to each other? We use a single equals sign to *assign* a value to a variable. Think about it from right to left: we assign the value on the right to the variable on the left. To check for *equality* we use two equal signs. The following might look a bit strange, but it works. We'll see a more "natural" use of this syntax in a bit.

```java
int val1 = 37;
int val2 = 23;
float val3 = 37.0000000000001;
boolean result = false;

result = val1 == val2;
result = val2 == val1;
result = val3 == val1;
```

So remember this: to **assign** a value to a variable, use a single equals sign, to test for **equality** between two variables or values, use two equals signs.

### Aside: `println`

Our only output so far has been through the drawing primitives that show up in the window that we created in the `setup()` function. But sometimes we need a bit more information about what's going on in our code. Wouldn't it be nice to know what values our variables have at different points in the code? Wouldn't that help us *debug* our program if we're having problems?

We can do that in Processing using the `println()` function. As the name suggests, it prints a line of code in the black *console* area at the bottom of the Processing window. If we want to print the phrase "Hello, world!" in the console area, we'd do the following:

```java
println("Hello, world!");
```

Note the double quotes around the text or *string* we want to print. We'll be using single quotes in a bit in a different, but related context. For right now, know that if you want to print out a series of *characters* (letters, numbers, punctuation, etc., in sum, a string), those characters need to be enclosed in double quotation marks.

You can also print the values of variables:

```java
int val1 = 32;
boolean toggle = false;
float temp = 23.4567;

println(val1 + " " + toggle + " " + temp);
```

See what I'm doing there? I can take each variable and concatenate it together with other variables and strings using the `+` operator. I'm using an idiom of "double quote space double quote" to format the resulting string nicely.

You can use `println()` even in the `draw()` function, which is updated every frame (and thus something is printed to the console every frame). Note that this might slow down your sketch if you're doing a lot of complicated drawing to the screen. When you submit (or finish) your sketch you should remove (or comment out) these extraneous `println()` statements. The only `println()` statements that should actually be printing anything be in your final code are statements that help you understand, in a general sense, what *state* your code is in. We'll be turning to this in your assignment for this week.

## Conditionals in Processing

We can now return to our definition of a conditional.

conditional

: The ability to perform different actions based off of whether or not a statement is true or false

In processing this is done using the `if`, `else if`, `else` construct. Let's look at an example.

```java
int val1 = 98;
int val2 = 23;

if (val1 > val2) {
    println("this will be printed");
}

if (val1 < val2) {
    println("this will not be printed");
}

if (val1 == val2) {
    println("this will not be printed");
}
```

Pretty straightforward, yes? In Processing you can use the `if` expression to *test* whether or not a statement is true or false. If it is `true`, then the statements in-between the brackets are evaluated.

But let's say that you'd like to sometime to happen when the `if` statement is false. There's a construct for that:

```java
int val1 = 98;
int val2 = 23;

if (val1 > val2) {
    println("this will be printed when val1 > val2");
} else {
    println("this will be printed when val1 <= val2");
}
```

So if `val1 > val2`, then the code in the first block is evaluated; if `val1 <= val2` then the code in the `else` block is evaluated.

In general, if you have an `if`, you should have an `else`, even if that else is there to tell you that you should never reach that part of your code. You'll thank yourself later if you do this.

Finally, let's say you've got a set of conditions that you'd like to test all at once. You can chain if statements together like the following:

```java
int val1 = 98;
int val2 = 23;
int val3 = 15;

if (val1 > val2) {
    println("val1 > val2");
} else if (val2 > val3) {
    println("val2 > val3");
} else if (val3 > val1) {
    println("val3 > val1");
} else {
    println("this will be printed when none of the conditions are met");
}

```

What's going on here? We can chain a series of `if` statements together with `else if`. But what *also* happens is once a single statement evaluates to `true`, the code within that block is evaluated, and then the rest of the `else if` statements are skipped.

We can use the Boolean operators as we described before in these `if` statements. It's best to be liberal in your use of parentheses to demarcate which variables are being used in which logic operations.

```java
int val1 = 98;
int val2 = 23;
int val3 = 15;

if ((val1 > val2) && (val2 > val3)) {
    println("val1 > val2 and val2 > val3");
} else {
    println("this will be printed when the 'and' operator evaluates to 'false'"); // Note what I'm doing here with quotes!
}
```

You can make these Boolean conditions as complicated as you'd like or as your program demands.

## Example: Dividing the window

We're going to start using conditionals to add interactivity to our code. This interactivity will allow us to create new aesthetic possibilities, or sometimes it'll just let the user change some parameters of our sketch.

Consider the following programming question:

> How might you change the background color of the window depending on whether or not the mouse was in the left hand or right hand part of the window?

Here's one way of doing it:

```java
void setup() {
  size(500, 500); // Let's work with a bigger window
  background(0);
  smooth(); // This turns on "anti-aliasing", which smooths out jagged edges
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  
  if (mouseX < (width/2)) {
    background(255);
  } else {
    background(0);
  }
}
```

And what about this, related problem?

> How might you change the background color of the window depending on whether or not the mouse was in the top or bottom half of the window?

A possible solution:

```java
void setup() {
  size(500, 500); // Let's work with a bigger window
  background(0);
  smooth(); // This turns on "anti-aliasing", which smooths out jagged edges
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  
  if (mouseY < (height/2)) {
    background(0, 255, 0);
  } else {
    background(0, 0, 255);
  }
}
```

And let's take a more complicated problem:

> Divide the window into four equally sized quadrants. The background color should be black, white, blue, and green when the mouse is in the top left, top right, bottom right, and bottom left quadrants, respectively.

This is one way of solving that problem:

```java
void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  
  if ((mouseX <= (width/2)) && (mouseY <= (height/2))) {
    background(0, 0, 0);
  } else if ((mouseX > (width/2)) && (mouseY <= (height/2))) {
    background(255, 255, 255);
  } else if ((mouseX > (width/2)) && (mouseY > (height/2))) {
    background(0, 0, 255);
  } else if ((mouseX <= (width/2)) && (mouseY > (height/2))) {
    background(0, 255, 0);
  } else {
    println("We should never get here");
  }
}
```

Finally, this is an exercise in the coordinate system:

> Divide the window into four equally sized quadrants. The color in each quadrant should be black, white, blue, and green when the mouse is in the top left, top right, bottom right, and bottom left quadrants, respectively.

```java
void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(127);

  if ((mouseX <= (width/2)) && (mouseY <= (height/2))) {
    fill(0);
    stroke(0);
    rect(0, 0, width/2, height/2);
  } else if ((mouseX > (width/2)) && (mouseY <= (height/2))) {
    fill(255);
    stroke(255);
    rect(width/2, 0, width, height/2);
  } else if ((mouseX > (width/2)) && (mouseY > (height/2))) {
    fill(0, 0, 255);
    stroke(0, 0, 255);
    rect(width/2, height/2, width, height);
  } else if ((mouseX <= (width/2)) && (mouseY > (height/2))) {
    fill(0, 255, 0);
    stroke(0, 255, 0);
    rect(0, height/2, width/2, height);
  } else {
    println("We should never get here");
  }
}
```

![Colored quardants based on location of mouse pointer.](media/resize/conditionals_squares.png)

## Example: key input

Last week we learned how to deal with mouse input through the `mousePressed()` function. There's a similar function for key presses, `keyPressed()`. Like with the mouse, there is a provided variable, `key`, that holds the value of the most recent key that was pressed.

We need to pause here and return to our discussion earlier about strings and characters. This actually turns out to be a complicated topic that we won't fully understand for a few weeks. Suffice it to say that in our `keyPressed()` function we're only going to be dealing with single *characters* this week; later weeks will dive into other keys such as the up and down arrows, and this mysterious difference between a *character* and a *string*.

Let's take our last example, where we were coloring each of the quadrants. The background defaulted to a medium grey. Let's say that you want the background of your window to be yellow when someone presses the "y" key. We see that in our `draw()` method the background is being redrawn each frame (which is necessary for this code to work). So we can't just set the background to yellow in `keyPressed()`, because it'll then be colored medium grey in the next frame. We need to save the state of the user's desire here. Let's create a new `boolean` variable at the top of our code called `boolean yellow`:

```java
boolean yellow = false;
```

Then we could write the following in our `keyPressed` method:

```java
void keyPressed() {
  // Note the single quotes and double equals sign!
  if (key == 'y') {
    yellow = true;  
  }
}
```

Note the single quotes! And double equals sign! Since we're testing whether `key` is equal to a single character, namely 'y', we have to phrase the statement in this way. So if someone presses the 'y' key, we then set `yellow` to be `true`.

Then we have one last thing to do. In our draw function, we need to color the background yellow when, well, `yellow` is true:

```java
  if (yellow) {
    background(255, 255, 0);
  } else {
    background(127);
  }
```

The complete code is the following:

```java
boolean yellow = false;

void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);

  if (yellow) {
    background(255, 255, 0);
  } else {
    background(127);
  }

  if ((mouseX <= (width/2)) && (mouseY <= (height/2))) {
    fill(0);
    stroke(0);
    rect(0, 0, width/2, height/2);
  } else if ((mouseX > (width/2)) && (mouseY <= (height/2))) {
    fill(255);
    stroke(255);
    rect(width/2, 0, width, height/2);
  } else if ((mouseX > (width/2)) && (mouseY > (height/2))) {
    fill(0, 0, 255);
    stroke(0, 0, 255);
    rect(width/2, height/2, width, height);
  } else if ((mouseX <= (width/2)) && (mouseY > (height/2))) {
    fill(0, 255, 0);
    stroke(0, 255, 0);
    rect(0, height/2, width/2, height);
  } else {
    println("We should never get here");
  }
}

void keyPressed() {
  // Note the single quotes and double equals sign!
  if (key == 'y') {
    yellow = true;
  }
}
```


![Key pressed, turning background yellow.](media/resize/conditionals_keyPressed.png)

Can you think of how to *toggle* our `yellow` variable, so that if someone types 'y' and `yellow` is false, `yellow` becomes true, and if they type 'y' again, `yellow` becomes false again?

## Example: Simple animation

We're going to start doing some computer animation work today. You might have done animation using software like Flash, Maya, or Unity in other courses. Here we're going to be doing everything from scratch, and because of that some of the animations might appear to be rather limited. But with some mathematical functions we can begin to get interesting imagery that might be difficult to create even in more "advanced" software.

Let's draw a simple circle on the far left of the screen, halfway down from the top.

```java
void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  ellipse(0, height/2, 10, 10);
}
```

Let's say that we'd like to have the circle move to the right one pixel every frame. How would we do that? What variable would we need to create? 

```java
int xPos = 0;

void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  ellipse(xPos, height/2, 10, 10);

  xPos = xPos + 1;
}
```

Awesome! We have a simple animation. Note that on line 15 we could have also used a special shorthand to *increment* the value of xPos: `xPos += 1`. This shorthand basically says, "add 1 to the value of xPos and assign it to xPos". If you wanted to increment xPos by 2, you'd write `xPos += 2`, and so on.

But if we let the circle go all the way to the right hand side of the screen, we see it disappear...never to be seen again. That's because `xPos` keeps on increasing without bound. What would we need to *check* to ensure that once it reaches the right hand edge of the screen it starts being drawn on the left hand edge again?

```java
int xPos = 0;

void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  ellipse(xPos, height/2, 10, 10);

  // This is another way of writing
  // xPos = xPos + 1;
  xPos += 1;

  if (xPos >= (width)) {
    xPos = 0;
  }
}
```

Great. Now we have a circle that moves from left to right, and when it reaches the right hand side of the screen, it starts from the left again.

<!-- *NB*: see the way in which we added `1` to xPos in the code above? You'll see the `+=` idiom quite often in Processing and other related programming languages. -->

What would we need to do if we wanted to speed up the circle's movement?

## Aside: Coding style

Before we go too much further, and before our programs become too long, let's talk about programming style. Consider the previous code with all leading whitespace removed:

```java
int xPos = 0;
void setup() {
size(500, 500);
background(0);
smooth();
}
void draw() {
stroke(255);
fill(255);
background(0);
ellipse(xPos, height/2, 10, 10);
// This is another way of writing
// xPos = xPos + 1;
xPos += 1;
if (xPos >= (width)) {
xPos = 0;
}
}
```

This code still runs properly, but it's not very readable. What about the following?

```java
int xPos = 0; void setup() { size(500, 500); background(0); smooth(); } void draw() { stroke(255); fill(255); background(0); ellipse(xPos, height/2, 10, 10); xPos += 1; if (xPos >= (width)) { xPos = 0; } }
```

We removed the comments, but everything else remained the same. Is this code readable? Does it run properly?

*Coding style* refers to the conventions that allow the code to be readable by other humans, *not* necessarily by the machine. Because the machine can still run code that, to us, is not very easy to read. So we decide on certain conventions of indentation, spacing, and so on, that allow us to more easily discover problems when they arise.

Each language has its own conventions, and even within particular languages there are different conventions amongst different programmers. Here are some conventions for this course:

* Indenting: when you start a new block, indent by two or four spaces, but keep your choice consistent throughout your code. The Processing editor defaults to two spaces, so that's a good value to stick with.
* Spacing: use empty lines to separate different parts of your code. At the least there should be a single empty line between your variable declarations and between each of your functions.
* Brackets: open brackets are on the same line as your function statement, if statement, loop statement, etc. Closing brackets should be in the same column as the first character of your opening statement.
* Comments: comment your code liberally.

I'm going to start grading on coding style with this assignment. We'll refine these guidelines as the semester progresses.

## Aside: parametric equations and trigonometry

Let's recall trigonometry and the `sin` and `cos` function.

![Sine and cosine functions. By Geek3 - Own work, CC BY 3.0, <https://commons.wikimedia.org/w/index.php?curid=10904768>.](media/resize/Sine_cosine_one_period.png)

<!-- By Geek3 - Own work, CC BY 3.0, https://commons.wikimedia.org/w/index.php?curid=10904768 -->

So when `x` is 0, `sin` is 0 and `cos` is 1. When `x` is `0.5*pi`, `sin` is 1 and `cos` is 0. And so on. Both `sin` and `cos` are *periodic* functions, which means that their outputs *repeat* at a regular interval known as the *period*.

What if we frame things this way?

```java
x = cos(t)
y = sin(t)
```

That is, we use a *parameter*, `t`, and evaluate it to get a value for `x` and `y`, which we could then think about as *coordinates*. We'd then get the following:

   t       x       y
------- ------- -------
   0       1       0
 0.5*pi    0       1
  pi      -1       0
 1.5*pi    0      -1
 2*pi      1       0


And this then draws out the *unit circle*:

![Unit circle. By Jim.belk - Own work, CC BY-SA 3.0, <https://commons.wikimedia.org/w/index.php?curid=12062595>.](media/resize/Unit_circle_angles_color.png)

<!-- By Jim.belk - Own work, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=12062595 -->

So, what have we learned? We can use a combination of trigonometric functions, parameterized in a certain way, to draw out the values of the unit circle. And that means that we can take combinations of `sin` and `cos` with an increasing parameter `t` (or some other increasing value) to draw out complicated shapes on the plane or within our window.

## Returning to our animation

When we last saw our animation it was a simple circle moving from left to right and then starting over again once it reached the right edge. Could we do something more interesting? Could we perhaps have the circle move up and down as it also moves from left to right? What function could we use for that? Yes, the `sin` function indeed. Let's think about what we'd need to do:

* The `sin` function returns values between 0 and 1, so we'd need to figure out how to make that a bigger range if we want to see the circle moving up and down
* We need some variable that increases over the lifetime of our sketch
* We may need to change the period of `sin` wave to be able to see the curve as it moves left to right

Some of these things we could get a good idea about ahead of time using the properties of trigonometric functions. But we don't need to remember all of that and just take some things for granted, at least for now.

First things first: there is a another provided variable, `frameCount`, that increases continuously and, as the name suggests, counts the number of frames since the sketch was started. So we have our continuously increasing parameter `t`.

We can then multiply the `sin` function by a constant value to increase its range. If we want the values to go from -30 to 30, we just multiply it by 30. 

And finally, we can multiply our parameter `t` by another factor to change the period of the `sin` function. In our case we'd like a factor less than 1.

So the last thing to do is to put everything together. We want this to change the `y` coordinate and have it move above and below the middle of our window. Here's what we end up with:

```java
int xPos = 0;

void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
 
  // This is the new code
  ellipse(xPos, 30*sin(frameCount*0.2) + height/2, 10, 10);
  
  // This is another way of writing
  // xPos = xPos + 1;
  xPos += 1;
  
  if (xPos >= (width)) {
    xPos = 0;  
  }
}
```

What happens if we add more circles, with different parameters?

### Translation

Up until now all of our code has assumed that the origin, `(0, 0)`, is at the top left of the screen. We can reset that origin with the `translate()` function before any set of drawing commands. This might be useful when using functions, like `sin` and `cos`, that might return negative values. So, to reset the origin so that the point in the middle of the window is `(0, 0)`, merely do:

```java
translate(width/2, height/2);
```

This is going to be useful for the assignment.

## Early computer art

The assignment for this week relates to some trends in early computer art from the 1960s and 1970s. This history could be a complete topic in and of itself, but I just want to note a few important figures. We should realize that some of these works were generated on a computer and then printed on a *plotter*[^plotter], where some of them were generated by a computer and then saved to video or film as animations.

[^plotter]: A plotter is basically a computer controlled pen or marker. The computer moves the paper on rails up and down and left and right above the paper, and then raises or lowers the marking implement in order to draw the marks on the paper.

### Vera Molnár

![Vera Molnár, Interruptions, 1968/69, open series, plotter drawing, 28.5 x 28.5 cm. More images of her work can be found at http://dam.org/artists/phase-one/vera-molnar/artworks-bodies-of-work/works-from-the-1960s-70s. ](media/molnar-works-from-the-1960s-70s-interruptions-2--dam-11051-SAu67-de-2.jpg)

Vera Molnár (1924--) is a Hungarian-born artist working in Paris. She started working with computers in the 1960s, programming the computer to vary geometric patterns in a regular fashion and then outputting the result on a plotter. In the words of the art historian and chronicler of early electronic and computer art Frank Popper, Molnár used the computer for its "technical promise"; its ability to "satisfy the desire for artistic innovation" and to "lighten the burden of traditional cultural forms"; to "encourage the mind to work in new ways"; and to measure the "physiological reactions of the audience" to works of computer art[@Popper1993a, 80].

### Lillian Schwartz

![*Pixillation* (1970; still), 4:00. Lillian Schwartz. For more of her films see http://lillian.com/films/.](media/resize/Schwartz-Pixillation-1970.png)

Lillian Schwartz (1927--) is an American artist who made some of the most iconic works of early computer art. Unlike Molnár, many of Schwartz' works are computer animations and outputted to film or video. Schwartz was a resident artist at Bell Laboratories, a major research center for not only advanced digital and computer technology, but also computer art and music. There she collaborated often with other artists such as Ken Knowlton. Schwartz' films from the early 1970s used both computer generated graphics and traditional analog post-production editing techniques. These films also often featured collaborations with leading computer musicians of the day such as Max Matthews and Jean-Claude Risset.

![*Mutations* (1972; still), 7:30. Lillian Schwartz. For more of her films see http://lillian.com/films/.](media/resize/Schwartz-Mutations-1972.png)


### John Whitney, Sr.

![*Matrix III* (1972; still), 10:34. John Whitney, Sr.](media/resize/Whitney-MatrixIII-1972.png)

John Whitney, Sr. (1917--1995) developed a repertoire of computer graphics techniques for both analog and digital computers. His exploration of visual effects, *Catalog* (1966), was extremely influential for computer graphics artists of the day. 

## Assignment

I've decided that it's best for assignments to be due on Thursday nights at 11:59:59PM. That way you have a bit more time to work on them, and also to meet with me if you have any questions.

Please upload your code into a new folder in your Sakai dropbox labeled `02_Conditionals`.

This assignment consists of two parts, **A** and **B**.

**A**. Consider the following `boolean` variable definitions and logical statements. What does `result` evaluate to in lines 13--17? Write your answers in a text file called LASTNAME_FIRSTNAME_boolean.

```java
boolean raining = true;
boolean snowing = false;
boolean sunny = true;
boolean spring = false;
boolean boots = false;
boolean raincoat = true;
boolean sloppy = true;
boolean puddle = true;
boolean mud = false;
boolean fun = true;
boolean result = false;

result = ((snowing && raining) || (spring || sloppy));
result = ((snowing && raining) || ((spring || raincoat) && (mud || fun)));
result = (((!fun) && (!mud)) || (puddle && boots));
result = ((spring || sunny) || (raining && puddle));
result = ((((((fun && fun) && fun) && fun) && fun) && fun) || mud);
```


![An example of what your code will draw.](media/resize/parametric_test.png)

**B**. We can extend the idea of parametric functions to draw lines that vary using combinations of `sin` and `cos`. We can then create pairs of `(x, y)` coordinates that outline the beginning and ending points of a line. You code will draw something like what you see in the Figure above.

Consider the following set of parametric equations. It's not important *how* these equations were determined, but only that they produce some interesting designs:

```java
x1 = 50*sin((t * frac)/5) + 80*cos((t * frac) + 1);
y1 = 100*cos((t * frac)/3) + 200*sin((t * frac));
    
x2 = 80*sin((t * frac)/3) + 100*cos((t * frac)/4);
y2 = 100*cos((t * frac)/3) + 40*sin((t * frac)/6 - 50);
```

So we've got a parameter `t` and `frac` variable (like in our animation examples earlier). Set `frac` to be `0.05` in order to get an image like the one you see above.

Your window size should be 600 by 600 pixels.

This is going to be a multipart assignment. When programming you want to start from the most basic thing and work your way towards the more difficult and/or advanced in terms of functionality. In this case, I've ordered things in terms of what should be done first, second, and so on.

1. Draw the line defined by the parametric equations above. The line starts with `(x1, y1)` and ends with `(x2, y2)`. The parameter `t` indicates that you'll be drawing multiple lines over time as the parameter increases; which increasing parameter you use is up to you[^parameter]. You'll probably want to define some variables at the beginning of the file. The stroke of the line should be a random grey between 175 and 255. And you'll want to translate the origin to the middle of the screen (see previous section).
2. When the user presses "e", the screen should be erased, and drawing should continue as before.
3. When the user presses "f", the lines should be drawn in a random color.
4. When the user presses "m", the lines should be drawn in the grey color as originally specified in part 1.
5. When the user clicks anywhere in the window, the origin should then be set to the location of the mouse click.
6. When the user presses "2", the parametric equations switches to a new "type" that you have defined based off of your experimentation. When the user presses "1" the sketch returns to the original equation.
7. When the user presses "s" the drawing of the sketch stops until the user presses "s" again.
8. When the user presses "r", all parameters are reset to their default values (origin at the middle, type is 1, color is monochrome).

[^parameter]: HINT: a very useful increasing parameter that you can use is `frameCount`.

Whenever there is a state change (i.e., when the user presses a key or clicks the mouse), this should be noted in the console using `println()`.

Please call your sketch LASTNAME_FIRSTNAME_parametric.

You are *required* to implement parts 1--4. Parts 5--7 are interesting extensions that you are *encouraged* to try and do. But please **do not** do them until you've attempted and/or completed parts 1--4. You will only be graded on parts 1--4.

Please also try to follow the coding guidelines described earlier in these notes.

I know that this appears to be a significant increase in complexity compared to the last assignment. I'd like to see how this works out, to challenge you. But I certainly don't want to loose anyone due to the challenge of this assignment. Please come to me if you have questions. Feel free to talk to others about the assignment. You're allowed to collaborate on *ideas* of how to solve these issues, but you *cannot* share specific code.

Refer to the last assignment for the guidelines on collaboration and the prohibition at looking at solutions from last year.

Again, I'm not intending this assignment to push people over the edge! But I'm also trying to titrate the difficulty of the assignments to the course, and I'd like to see how you all respond to something of some difficulty. If it doesn't work out this week, I'll change the difficulty of the next assignment. **Please do not worry too much about your grade when working on the assignment.** I will be generous in my partial credit, especially if you add comments in your code as to how you *wanted* to solve the problem.
